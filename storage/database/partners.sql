-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th3 15, 2021 lúc 04:30 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ouransoft`
--

--
-- Đang đổ dữ liệu cho bảng `partners`
--

INSERT INTO `partners` (`id`, `name`, `image`, `link`, `updated_at`, `created_at`) VALUES
(18, 'LG display', '1615530832.jpg', 'Lgs.com', '2021-03-12 00:42:49', '2021-03-11 23:33:52'),
(22, 'FPT', '1615653850.png', 'fpt.com', '2021-03-13 09:44:10', '2021-03-13 09:44:10'),
(23, 'google', '1615653896.jpg', 'google.com', '2021-03-13 09:44:56', '2021-03-13 09:44:56'),
(24, 'VNG', '1615653919.jpg', 'vng.com', '2021-03-13 09:45:19', '2021-03-13 09:45:19'),
(25, 'cpn', '1615698599.png', 'cpn.com', '2021-03-13 22:09:59', '2021-03-13 22:09:59'),
(26, 'DELL', '1615698640.png', 'dell.com', '2021-03-13 22:10:41', '2021-03-13 22:10:41'),
(27, 'VNP', '1615698656.jpg', 'vnp.com', '2021-03-13 22:10:56', '2021-03-13 22:10:56'),
(28, 'HP', '1615698673.jpg', 'hp.com', '2021-03-13 22:11:13', '2021-03-13 22:11:13');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
