-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th3 29, 2021 lúc 03:19 AM
-- Phiên bản máy phục vụ: 10.4.17-MariaDB
-- Phiên bản PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ouransoft`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Nguyen Tuan anh', 'tuananh31020@gmail.com', 'a', '2021-03-16 21:57:53', '2021-03-16 21:57:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `documents`
--

INSERT INTO `documents` (`id`, `title`, `image`, `file`, `ordering`, `created_at`, `updated_at`) VALUES
(2, 'anh3', '1615957173.jpg', '1615957173.jpg,', 1, '2021-03-16 21:59:33', '2021-03-16 21:59:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` smallint(6) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`id`, `title`, `ordering`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Trang chủ', 1, '/', '2021-03-16 20:40:18', '2021-03-16 20:40:18'),
(2, 'Giới thiệu', 2, 'introduce_company', '2021-03-16 20:40:28', '2021-03-17 00:34:06'),
(3, 'Đào tạo', 3, 'training', '2021-03-16 20:40:37', '2021-03-17 00:34:11'),
(4, 'Tin tức', 4, 'news', '2021-03-16 20:40:48', '2021-03-17 00:34:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` smallint(6) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'news-banner1.jpg',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metatitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakeyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_public` timestamp NOT NULL DEFAULT current_timestamp(),
  `timer` date NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `title`, `category`, `description`, `ordering`, `image`, `content`, `metatitle`, `metadescription`, `metakeyword`, `date_public`, `timer`, `created_at`, `updated_at`) VALUES
(4, '9x chuyển nghề lập trình viên Blockchain nhờ học trực tuyến', 'a', 'mô tả 1', 1, '1615976028.jpg', '<p>g</p>\r\n\r\n<p>&nbsp;</p>', 'b', 'c', 'c2', '2021-03-17 07:15:38', '2021-03-19', '2021-03-17 00:15:38', '2021-03-17 03:13:48'),
(7, 'Chuyển đổi số trong doanh nghiệp khoa học công nghệ', 'tin vắn', 'mô tả 1', 2, '1615975724.jpg', '<p>jjlk</p>', 'v', 'c', 'c2', '2021-03-17 07:21:28', '2021-03-17', '2021-03-17 00:21:28', '2021-03-17 03:11:07'),
(9, 'sư', 's', 's', 3, '1615975653.jpg', '<p>&nbsp;</p>\r\n\r\n<h1>Chiếm t&agrave;i khoản của Bill Gates, hacker 17 tuổi nhận &aacute;n t&ugrave;</h1>\r\n\r\n<h1>Hacker trẻ đ&atilde; x&acirc;m nhập v&agrave;o t&agrave;i khoản Twitter của nhiều người nổi tiếng như Elon Musk, Barack Obama, Joe Biden... để lừa đảo.</h1>\r\n\r\n<p><em>The Verge&nbsp;</em>đưa tin tin tặc Twitter tuổi teen Graham Ivan Clark đ&atilde; nhận tội trong vụ lừa đảo Bitcoin chưa từng c&oacute;, xảy ra v&agrave;o ng&agrave;y 15/7/2020.</p>\r\n\r\n<p>Theo hồ sơ được đệ tr&igrave;nh l&ecirc;n t&ograve;a &aacute;n Florida, vụ chiếm đoạt n&agrave;y li&ecirc;n quan đến h&agrave;ng chục t&agrave;i khoản của người nổi tiếng tr&ecirc;n mạng x&atilde; hội Twitter.</p>\r\n\r\n<table align=\"center\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"Hacker tre tuoi ngoi tu 3 nam anh 1\" src=\"https://znews-photo.zadn.vn/w1024/Uploaded/ycwkaivo/2021_03_17/e143d3d4_f9bd_470b_9288_0c0774caf204_1920x1080.jpg\" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Clark bị tuy&ecirc;n &aacute;n 3 năm cho h&agrave;nh vi chiếm đoạt t&agrave;i khoản v&agrave; lừa đảo của m&igrave;nh. Ảnh:&nbsp;<em>The Verge.</em></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Clark, 17 tuổi, bị buộc tội cầm đầu vụ lừa đảo v&agrave; sẽ phải ngồi t&ugrave; 3 năm. T&iacute;nh đến ng&agrave;y tuy&ecirc;n &aacute;n, Clark đ&atilde; bị tạm giam 229 ng&agrave;y, như vậy, hacker trẻ tuổi n&agrave;y vẫn phải bị giam th&ecirc;m 866 ng&agrave;y nữa.</p>\r\n\r\n<p>V&agrave;o thời điểm vụ việc xảy ra, t&agrave;i khoản Twitter của những người nổi tiếng như Elon Musk, Bill Gates, Barack Obama v&agrave; cả Joe Biden đ&atilde; bị chiếm quyền điều khiển, đăng th&ocirc;ng điệp nhận Bitcoin để chiếm đoạt hơn&nbsp;100.000 USD.</p>\r\n\r\n<p>Theo h&atilde;ng th&ocirc;ng tấn&nbsp;<em>Tampa Bay Times</em>, c&aacute;c c&ocirc;ng tố vi&ecirc;n đ&atilde; quyết định một mức &aacute;n nhẹ, dựa tr&ecirc;n độ tuổi chưa th&agrave;nh ni&ecirc;n của Clark, đồng thời cho ph&eacute;p anh c&oacute; thể chấp h&agrave;nh bản &aacute;n của m&igrave;nh tại một trại gi&aacute;o dưỡng.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, hồ sơ ghi r&otilde; Clark sẽ kh&ocirc;ng được sử dụng m&aacute;y t&iacute;nh khi kh&ocirc;ng được cấp ph&eacute;p hoặc khi kh&ocirc;ng c&oacute; sự gi&aacute;m s&aacute;t của c&aacute;c cơ quan thực thi ph&aacute;p luật.</p>\r\n\r\n<table align=\"center\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"Hacker tre tuoi ngoi tu 3 nam anh 2\" src=\"https://znews-photo.zadn.vn/w1024/Uploaded/ycwkaivo/2021_03_17/twitterhack071620v2_960x540.jpg\" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Elon Musk v&agrave; Barack Obama l&agrave; những nạn nh&acirc;n nổi tiếng bị x&acirc;m nhập t&agrave;i khoản Twitter. Ảnh:&nbsp;<em>Google.</em></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Theo&nbsp;<em>The Verge,&nbsp;</em>vụ hack của Clark l&agrave; một trong những sự cố an ninh mạng tồi tệ nhất trong lịch sử Twitter.</p>\r\n\r\n<p>&nbsp;</p>', 's', 's', 's', '2021-03-17 09:51:35', '2021-03-17', '2021-03-17 02:51:35', '2021-03-17 03:07:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `partners`
--

INSERT INTO `partners` (`id`, `name`, `image`, `link`, `created_at`, `updated_at`) VALUES
(1, 'tuan anh', '1615972009.jpg', 'trangchu.php', '2021-03-16 20:26:48', '2021-03-17 02:06:49'),
(2, 'Google', '1615973903.jpg', 'gg.com', '2021-03-17 02:38:23', '2021-03-17 02:38:23'),
(3, 'FPT', '1615973912.png', 'fpt.com', '2021-03-17 02:38:32', '2021-03-17 02:38:32'),
(4, 'DELL', '1615973919.png', 'dell.com', '2021-03-17 02:38:39', '2021-03-17 02:38:39'),
(5, 'VNP', '1615973933.jpg', 'vnp.com', '2021-03-17 02:38:53', '2021-03-17 02:38:53'),
(6, 'HP', '1615973951.jpg', 'hp.com', '2021-03-17 02:39:11', '2021-03-17 02:39:11'),
(7, 'VNG', '1615973963.jpg', 'vng.com', '2021-03-17 02:39:23', '2021-03-17 02:39:23'),
(8, 'SAMSUNG', '1615974001.jpg', 'samsung.com', '2021-03-17 02:40:01', '2021-03-17 02:40:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', '$2y$10$kzP0kyIgZJEfu9RXl8JSSehmX4kf03hO59Ers4AcZSVEiITdAtFye', '2021-03-19 18:16:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `route`, `created_at`, `updated_at`) VALUES
(2, 'webconfig_edit', 'admin.webconfig.edit', NULL, NULL),
(3, 'webconfig_update', 'admin.webconfig.update', NULL, NULL),
(4, 'subcriber_view', 'admin.subcriber', NULL, NULL),
(5, 'subcriber_delete', 'admin.subcriber.delete', NULL, NULL),
(6, 'partner_view', 'admin.partner', NULL, NULL),
(7, 'partner_insert', 'admin.partner.insert', NULL, NULL),
(8, 'partner_edit', 'admin.partner.edit', NULL, NULL),
(9, 'partner_update', 'admin.partner.update', NULL, NULL),
(10, 'partner_delete', 'admin.partner.delete', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_roles`
--

CREATE TABLE `permission_roles` (
  `permission_id` int(11) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `permission_roles`
--

INSERT INTO `permission_roles` (`permission_id`, `role_id`) VALUES
(1, 52),
(6, 52);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `content`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Mado-car', '1615971053.png', 'Chuyên đáp ứng dịch vụ, nhu cầu cho chuyên viên cao cấp', 'Mobile Application', '2021-03-17 01:50:53', '2021-03-17 02:43:03'),
(2, 'Kaizen', '1615973817.png', 'Giúp doanh nghiệp huấn luyện chuẩn hóa nhận thức về PDCA', 'Website Application', '2021-03-17 02:36:57', '2021-03-17 02:36:57'),
(3, 'Mado-car', '1615973832.png', 'Chuyên đáp ứng dịch vụ, nhu cầu  cho chuyên viên cao cấp', 'Mobile Application', '2021-03-17 02:37:12', '2021-03-17 02:37:12');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `updated_at`, `created_at`) VALUES
(2, 'Admin', 'Quản lí\r\n', NULL, NULL),
(3, 'Editor', 'Biên tập viên', NULL, NULL),
(20, 'admin2', 'quản lí sản phẩm, đối tác', '2021-03-25 21:40:08', '2021-03-25 21:40:08'),
(50, 'Mado-car', 'Người viết bài', '2021-03-28 18:06:31', '2021-03-28 18:06:31'),
(51, 'LG display', 'Người viết bài', '2021-03-28 18:12:09', '2021-03-28 18:12:09'),
(52, 'dsa', 'Người viết bài', '2021-03-28 18:17:52', '2021-03-28 18:17:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_user`
--

CREATE TABLE `role_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(38, 2),
(38, 3),
(39, 2),
(39, 3),
(39, 4),
(39, 5),
(39, 6),
(39, 7),
(39, 8),
(39, 9),
(39, 10),
(40, 2),
(40, 3),
(41, 2),
(41, 3),
(42, 2),
(43, 2),
(43, 3),
(43, 4),
(44, 2),
(46, 3),
(47, 2),
(47, 3),
(48, 3),
(49, 3),
(49, 4),
(49, 5),
(49, 6),
(49, 2),
(45, 2),
(50, 11),
(55, 3),
(65, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `subcribers`
--

CREATE TABLE `subcribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `subcribers`
--

INSERT INTO `subcribers` (`id`, `date`, `email`, `created_at`, `updated_at`) VALUES
(1, '2021-03-17 04:56:57', 'tuananh31020@gmail.com', '2021-03-16 21:56:57', '2021-03-16 21:56:57');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(45, 'admin', 'admin@gmail.com', NULL, '$2y$10$KsNyDLVgRO6IB0bwLraCtuQRm2vrEpRHjcaPsq7vUxOr7z./9HkRa', NULL, '2021-03-21 20:39:00', '2021-03-24 21:42:40'),
(46, 'admin1', 'admin1@gmail.com', NULL, '$2y$10$gfvYa2mIXyb2BD2RAZBji.s.7GIn9GnSfzwLsKsUDo6/RJpZxh09S', NULL, '2021-03-21 20:44:03', '2021-03-24 21:43:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `web_configs`
--

CREATE TABLE `web_configs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `favicon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `web_configs`
--

INSERT INTO `web_configs` (`id`, `name`, `logo`, `favicon`, `address`, `tel`, `description`, `sub_description`, `facebook`, `twitter`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'trang chủ', '1615973388.png', '1615972800.png', 'Số 3, lô Mở rộng, Trung Hành 5 Đằng Lâm, Hải An, Hải Phòng', '0833.993.966', '323232323', '32323', '#', '#', '#', NULL, '2021-03-18 23:43:59');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `subcribers`
--
ALTER TABLE `subcribers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `web_configs`
--
ALTER TABLE `web_configs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT cho bảng `subcribers`
--
ALTER TABLE `subcribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT cho bảng `web_configs`
--
ALTER TABLE `web_configs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
