-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th3 15, 2021 lúc 04:32 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ouransoft`
--

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `content`, `description`, `updated_at`, `created_at`) VALUES
(21, 'Mado-car', '1615705966.png', 'Chuyên đáp ứng dịch vụ, nhu cầu  cho chuyên viên cao cấp', 'Mobile Application', '2021-03-14 00:16:31', '2021-03-14 00:12:46'),
(23, 'Kaizen', '1615772031.png', 'Giúp doanh nghiệp huấn luyện chuẩn hóa nhận thức về PDCA', 'Website Application', '2021-03-14 18:33:51', '2021-03-14 18:33:51'),
(24, 'Mado-car', '1615772052.png', 'Chuyên đáp ứng dịch vụ, nhu cầu cho chuyên viên cao cấp', 'Mobile Application', '2021-03-14 18:34:12', '2021-03-14 18:34:12');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
