<!-- Hero Start -->
<section class="bg-half bg-light d-table w-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title"> {{__('translate.Kết quả tìm kiếm cho')}}: <span class="search-text">"{{$result}}"</span></h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="/">{{__('translate.Trang chủ')}}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{__('translate.Tìm kiếm')}}</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<div class="news-detail-wrap my-5 mx-auto row container">
    <div class="col-sm-8 search-result">
        @foreach($records as $record)
            <div class="result-box mx-auto mb-5 px-4 py-4 bg-light rounded shadow">
                <div class="time">
                    <small class="date text-org">
                        <i class="uil uil-calendar-alt"></i>
                        <span>{{$record->timer}}</span>
                    </small>
                </div>
                <div class="mb-3">
                    <a href="/news_detail/{{$record->id}}">
                        <h4 class="text-dark search-title">{{$record->title}}</h4>
                    </a>
                </div>
                <div class="content">
                    {{$record->description}}
                </div>
            </div>
        @endforeach
    </div>
    <!-- START SIDEBAR -->
    <div class="col-lg-4 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
        <div class="card border-0 sidebar sticky-bar rounded shadow">
            <div class="card-body">
                <!-- SEARCH -->
                <div class="widget">
                    <form action="{{url('/search')}}" method="post">
                        {{csrf_field()}}
                        <div class="input-group mb-3 border rounded">
                            <input type="text" id="s" name="search-value" class="form-control border-0" placeholder="{{__('translate.Tìm kiếm')}}...">
                            <button type="submit" class="input-group-text bg-transparent border-0" id="searchsubmit"><i class="uil uil-search text-org"></i></button>
                        </div>
                    </form>
                </div>
                <!-- SEARCH -->

                <!-- RECENT POST -->
                <div class="widget mb-4 pb-2">
                    <h5 class="widget-title">{{__('translate.Bài viết mới nhất')}}</h5>
                    <div class="mt-4">
                        @foreach($news as $record)
                            <div class="clearfix post-recent">
                                <div class="post-recent-thumb float-start"> <a href="/news_detail/{{$record->id}}"> <img alt="img" src="/upload/news/image/{!!$record->image!!}" class="img-fluid rounded"></a></div>
                                <div class="post-recent-content float-start"><a href="/news_detail/{{$record->id}}">{{$record->title}}</a><span class="text-muted mt-2">{{$record->timer}}</span></div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- RECENT POST -->
            </div>
        </div>
    </div><!--end col-->
    <!-- END SIDEBAR -->
</div>
