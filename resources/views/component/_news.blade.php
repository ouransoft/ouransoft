<section class="section bg-light">
    <div class="container">
        <div class="row align-items-center mb-4 pb-2">
            <div class="col-lg-6">
                <div class="section-title text-lg-start">
                    <h4 class="title mb-4 mb-lg-0">{{trans('translate.Tin tức')}}</h4>
                    <a href="{{route('news')}}" class="mt-3 h6 text-org">{{trans('translate.Xem tất cả')}} <i class="uil uil-angle-right-b align-middle"></i></a>
                </div>
            </div><!--end col-->

            <div class="col-lg-6">
                <div class="section-title text-center text-lg-start">
                    <p class="text-muted mb-0 mx-auto para-desc">{{trans('translate.Cập nhật những')}} <span class="text-org fw-bold">{{trans('translate.tin tức, lịch trình sự kiện')}}</span> {{trans('translate.mới nhất')}}.</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            @foreach($records_news as $record)
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow h-100">
                        <div class="position-relative">
                            <img src="/upload/news/image/{{$record->image}}" class="card-img-top rounded-top" style="height: 200px">
                        <div class="overlay rounded-top bg-dark"></div>
                        </div>
                        <div class="card-body content">
                            <h5><a href="/news_detail/{{$record->id}}" class="card-title title text-dark">{{$record->title}}</a></h5>
                            <div class="post-meta d-flex justify-content-between mt-3">
                                <a href="/news_detail/{{$record->id}}" class="text-muted readmore">{{trans('translate.Đọc thêm')}} <i class="uil uil-angle-right-b align-middle"></i></a>
                            </div>
                        </div>
                        <div class="author">
                            <small class="text-light date"><i class="uil uil-calendar-alt"></i> {{$record->timer}}</small>
                        </div>
                    </div>
                </div><!--end col-->
            @endforeach
        </div><!--end row-->
    </div>
</section>
