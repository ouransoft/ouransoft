<section class="section border-bottom bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">{{trans('translate.Về Chúng Tôi')}}</h4>
                    <p class="text-muted para-desc mb-0 mx-auto"><span class="text-org fw-bold">{{trans('translate.Định hướng phát triển thành công ty công nghệ hàng đầu Việt Nam')}}</span></p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6 mt-4 pt-2 px-0">
                <img src="{{asset('upload/ouransoft-star-white-2.png')}}" alt="ảnh" style="width: 100%">
            </div><!--end col-->

            <div class="col-lg-6 col-md-6 mt-4 pt-2">
                <div class="section-title ms-lg-5">
                    <h4 class="title mb-4">{{trans('translate.Tầm nhìn & Sứ mệnh')}}</h4>
                    <p class="">{{trans('translate.Không ngừng đổi mới, sáng tạo để kiến tạo hệ sinh thái các sản phẩm dịch vụ đẳng cấp, góp phần nâng tầm vị thế của thương hiệu Việt trên trường quốc tế')}}.</p>
                    <ul class="list-unstyled text-muted">
                        <li class="mb-0"><span class="text-org h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>{{trans('translate.Lắng nghe, thấu hiểu và đồng cảm với những nhu cầu của quý khách hàng, từ đó cung cấp chính xác giải pháp công nghệ, các công cụ quản trị đơn giản, thông minh và hiệu quả nhất tới tay khách hàng vào đúng thời điểm')}}.</li>
                        <li class="mb-0"><span class="text-org h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>{{trans('translate.Tạo môi trường phát triển bản thân và nuôi dưỡng những ước mơ cho đồng nghiệp')}}.</li>
                        <li class="mb-0"><span class="text-org h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>{{trans('translate.Nâng cao giá trị gia tăng nhiều hơn cho các nhà đầu tư và các đối tác')}}.</li>
                    </ul>
                    <a href="{{route('introduce')}}" class="mt-3 h6 text-org">{{trans('translate.Tìm hiểu thêm')}} <i class="uil uil-angle-right-b align-middle"></i></a>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->

</section><!--end section-->
