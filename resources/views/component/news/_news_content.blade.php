<!-- Hero Start -->
<section class="bg-half bg-light d-table w-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title">{{__('translate.Tin tức')}}</h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="#">{{__('translate.Trang chủ')}}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{__('translate.Tin tức')}}</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- Blog STart -->
<section class="section">
    <div class="container">
        <div class="row">
            @foreach($records->sortByDesc('date_public') as $record)
                <div class="col-lg-4 col-md-6 mb-4 pb-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden h-100">
                        <div class="position-relative">
                            <img src="/upload/news/image/{{$record->image}}" class="card-img-top" style="height: 200px">
                            <div class="overlay rounded-top bg-dark"></div>
                        </div>
                        <div class="card-body content">
                            <h5><a href="/news_detail/{{$record->id}}" class="card-title title text-dark">{{$record->title}}</a></h5>
                            <div class="post-meta d-flex justify-content-between mt-3">
                                <a href="/news_detail/{{$record->id}}" class="text-muted readmore">{{__('translate.Đọc thêm')}} <i class="uil uil-angle-right-b align-middle"></i></a>
                            </div>
                        </div>
                        <div class="author">
                            <small class="text-light date"><i class="uil uil-calendar-alt"></i> {{$record->timer}}</small>
                        </div>
                    </div>
                </div><!--end col-->
            @endforeach
            <!-- PAGINATION START -->
            <div class="col-12">
                {{-- <ul class="pagination justify-content-center mb-0">
                    <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Previous">Prev</a></li>
                    <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Next">Next</a></li>
                </ul> --}}
                <div class="d-flex flex-row justify-content-center">
                    {{ $records->links('vendor.pagination.custom') }}
                </div>
            </div><!--end col-->
            <!-- PAGINATION END -->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- Blog End -->
