<!-- Hero Start -->
<section class="bg-half bg-light d-table w-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title"> {{__('translate.Bài viết')}} </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="/">{{__('translate.Trang chủ')}}</a></li>
                                <li class="breadcrumb-item"><a href="{{route('news')}}">{{__('translate.Tin tức')}}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{__('translate.Bài viết')}}</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- Blog STart -->
<section class="section">
    <div class="container">
        <div class="row">
            <!-- BLog Start -->
            <div class="col-lg-8 col-md-6">
                <h3>{{$records->title}}</h3>
                <p class="text-muted">
                    <i class="mdi mdi-calendar-check"></i>
                    {{$records->date_public}}
                </p>
                <div class="card blog blog-detail border-0 shadow rounded">
                    <img src="/upload/news/image/{!!$records->image!!}" class="img-fluid rounded-top" alt="">
                    <div class="card-body content">
                        <p class="mt-3">{!!$records->content!!}</p>
                        {{-- <blockquote class="blockquote mt-3 p-3">
                            <p class="text-muted mb-0 fst-italic">Nguồn: https://www.24h.com.vn/cong-nghe-thong-tin/cha-de-phan-mem-adobe-qua-doi-c55a1244608.html</p>
                        </blockquote> --}}
                    </div>
                </div>
            </div>
            <!-- BLog End -->

            <!-- START SIDEBAR -->
            <div class="col-lg-4 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="card border-0 sidebar sticky-bar rounded shadow">
                    <div class="card-body">
                        <!-- SEARCH -->
                        <div class="widget">
                            <form action="{{url('/search')}}" method="post">
                                {{csrf_field()}}
                                <div class="input-group mb-3 border rounded">
                                    <input type="text" id="s" name="search-value" class="form-control border-0" placeholder="{{__('translate.Tìm kiếm')}}...">
                                    <button type="submit" class="input-group-text bg-transparent border-0" id="searchsubmit"><i class="uil uil-search text-org"></i></button>
                                </div>
                            </form>
                        </div>
                        <!-- SEARCH -->

                        <!-- RECENT POST -->
                        <div class="widget mb-4 pb-2">
                            <h5 class="widget-title">{{__('translate.Bài viết mới nhất')}}</h5>
                            <div class="mt-4">
                                @foreach($news as $record)
                                    <div class="clearfix post-recent">
                                        <div class="post-recent-thumb float-start"> <a href="/news_detail/{{$record->id}}"> <img alt="img" src="/upload/news/image/{!!$record->image!!}" class="img-fluid rounded"></a></div>
                                        <div class="post-recent-content float-start"><a href="/news_detail/{{$record->id}}">{{$record->title}}</a><span class="text-muted mt-2">{{$record->timer}}</span></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- RECENT POST -->
                    </div>
                </div>
            </div><!--end col-->
            <!-- END SIDEBAR -->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- Blog End -->
