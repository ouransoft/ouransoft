<!-- Navbar STart -->
<header id="topnav" class="defaultscroll sticky d-flex flex-column">
    <a class="logo1 mx-auto d-flex align-items-end" href="/">
        <img src="/upload/webconfig/{{$web->logo}}" class="h-75" alt="logo">
    </a>
    <div class="container">
        <!-- Logo container-->
        <a class="logo mx-auto" href="/">
            <img src="/upload/webconfig/{{$web->logo}}" height="34" alt="logo">
        </a>

        <div class="menu-extras">
            <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle" id="isToggle" onclick="toggleMenu()">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </div>
        </div>

        <ul class="buy-button list-inline mb-0">
            <li class="list-inline-item mb-0">
                <div class="dropdown">
                    <button type="button" class="btn btn-link text-decoration-none dropdown-toggle p-0 pe-2" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="uil uil-search" style="color: #fff"></i>
                    </button>
                    <div class="dropdown-menu dd-menu dropdown-menu-end bg-white shadow rounded border-0 mt-3 py-0" style="width: 300px;">
                        <form action="{{url('/search')}}" method="post" class="d-flex flex-row">
                            @csrf
                            <input type="text" id="text" name="search-value" class="form-control border bg-white" placeholder="Tìm kiếm...">
                            <button type="submit" class="search-btn btn btn-link text-decoration-none dropdown-toggle p-0 pe-2">
                                <i class="uil uil-search text-org pr-2"></i>
                            </button>
                        </form>
                    </div>
                 </div>
            </li>
        </ul>
        <!-- End Logo container-->

        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                @foreach($menu as $record)
                    <li class="mx-2 {{ Request::is($record->link) ? 'active' : '' }}">
                        <a href="{{url($record->link)}}" class="sub-menu-item fix {{ Request::is($record->link) ? 'active' : '' }}">{{trans('translate.'.$record->title)}}</a>
                    </li>
                @endforeach
                <li class="has-submenu parent-menu-item">
                    <a href="javascript:void(0)">{{trans('translate.Sản phẩm')}}</a><span class="menu-arrow"></span>
                    <ul class="submenu">
                        <li><a href="http://trainghiem.ouransoft.vn/" target="_blank" class="sub-menu-item">i-VIBO</a></li>
                        <li><a href="http://madocar.vn/" class="sub-menu-item">Madocar</a></li>
                        <li><a href="javascript:void(0)" class="sub-menu-item">AIX</a></li>
                        <li><a href="javascript:void(0)" class="sub-menu-item">Car autocare</a></li>
                    </ul>
                </li>
                <li class="has-submenu parent-menu-item">
                    <a href="javascript:void(0)">{{trans('translate.Hỗ trợ')}}</a><span class="menu-arrow"></span>
                    <ul class="submenu">
                        <li><a href="{{route('faqs')}}" class="sub-menu-item">FAQS</a></li>
                    </ul>
                </li>
                <li class="has-submenu parent-menu-item">
                    <a href="javascript:void(0)">{{__('translate.Ngôn ngữ')}}</a><span class="menu-arrow"></span>
                    <ul class="submenu">
                        <li><a href="lang/vi" class="sub-menu-item">{{trans('translate.Tiếng Việt')}}</a></li>
                        <li><a href="lang/en" class="sub-menu-item">{{trans('translate.Tiếng Anh')}}</a></li>
                        <li><a href="lang/jp" class="sub-menu-item">{{trans('translate.Tiếng Nhật')}}</a></li>
                    </ul>
                </li>
            </ul><!--end navigation menu-->
        </div><!--end navigation-->
    </div><!--end container-->
</header><!--end header-->
<!-- Navbar End -->
