<section class="section pb-3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">{{trans('translate.Sản phẩm')}}</h4>
                    <p class="text-muted para-desc mx-auto mb-0"><span class="text-org fw-bold">{{trans('translate.Ouransoft cam kết đưa ra những sản phẩm ứng dụng chất lượng nhất')}}.</span></p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-12 mt-4">
                <div class="my-sliders">
                    @foreach($records_product as $record)
                    <div class="col-lg-4 col-md-6 mt-4 pt-2">
                        <a href="{{$record->link!=null ? $record->link : 'javascript:void(0)'}}" class="text-dark" target="{{$record->link!=null ? '__blank' : ''}}">
                            <div class="rounded shadow m-2 p-4 d-flex flex-column justify-content-between" style="height: 270px; background: #5b6c83">
                                <div class="d-flex flex-row align-items-center justify-content-center">
                                    <img src="/upload/product/{{$record->image}}" alt="img" class="mr-2" style="width: 16%; margin-right: 5px">
                                    <h4 class="mb-0">{{$record->name}}</h4>
                                </div>
                                <p class="mt-4 text-center" style="color: #d0deec">"{{trans('translate.'.$record->content)}}"</p>
                                <h6 class="text-org text-center">- {{$record->description}} -</h6>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
