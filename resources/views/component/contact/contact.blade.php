<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- favicon -->
    <link rel="shortcut icon" href="{{asset('upload/logo-ouransoft.png')}}">
    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="{{asset('css/materialdesignicons.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('unicons.iconscout.com/release/v3.0.6/css/line.css')}}">
    <!-- Slider -->
    <link rel="stylesheet" href="{{asset('css/tiny-slider.css')}}"/>
    <!-- Main Css -->
    <link href="{{asset('css/style.dark.min.css')}}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="{{asset('css/colors/default.css')}}" rel="stylesheet" id="color-opt">
    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <title>Liên hệ</title>
</head>
<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <!-- Loader -->

    @include('component/_navbar')
    @include('component/contact/_contact')
    @include('component/_footer')

    <!-- Back to top -->
    <a href="#" onclick="topFunction()" id="back-to-top" class="btn btn-icon btn-org back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
    <!-- Back to top -->

    <!-- javascript -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- SLIDER -->
    <script src="{{asset('js/tiny-slider.js')}}"></script>
    <!-- Icons -->
    <script src="{{asset('js/feather.min.js')}}"></script>
    <!-- Switcher -->
    <script src="{{asset('js/switcher.js')}}"></script>
    <!-- Main Js -->
    <script src="{{asset('js/plugins.init.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>
