<section class="section pt-5 mt-100">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-0">
                <div class="card map border-0">
                    <div class="card-body p-0">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3728.875378471981!2d106.71002466457627!3d20.836738249682437!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314a7b6509426d93%3A0xfd6116d81e29e75d!2sOURANSOFT!5e0!3m2!1sen!2s!4v1621908168178!5m2!1sen!2s"
                            style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->

    <div class="container mt-100 mt-60">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0 order-2 order-md-1">
                <div class="card custom-form rounded border-0 shadow p-4">
                    <form id="contact__insert">
                        @csrf
                        <p id="error-msg" class="mb-0"></p>
                        <div id="simple-msg"></div>
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-3">
                                    <label class="form-label">{{__('translate.Họ tên')}}<span class="text-danger">*</span></label>
                                    <div class="form-icon position-relative">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-user fea icon-sm icons">
                                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="12" cy="7" r="4"></circle>
                                        </svg>
                                        <input name="name" id="name" type="text" class="form-control ps-5"
                                            placeholder="{{__('translate.Nhập tên')}}..">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-3">
                                    <label class="form-label">{{__('translate.Email')}} <span class="text-danger">*</span></label>
                                    <div class="form-icon position-relative">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-mail fea icon-sm icons">
                                            <path
                                                d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z">
                                            </path>
                                            <polyline points="22,6 12,13 2,6"></polyline>
                                        </svg>
                                        <input name="email" id="email" type="email" class="form-control ps-5"
                                            placeholder="{{__('translate.Nhập email')}}..">
                                    </div>
                                </div>
                            </div>
                            <!--end col-->

                            <div class="col-12">
                                <div class="mb-3">
                                    <label class="form-label">{{__('translate.Tin nhắn')}}<span class="text-danger">*</span></label>
                                    <div class="form-icon position-relative">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-message-circle fea icon-sm icons clearfix">
                                            <path
                                                d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z">
                                            </path>
                                        </svg>
                                        <textarea name="content" id="comments" rows="4" class="form-control ps-5"
                                            placeholder="{{__('translate.Nhập tin nhắn')}}.."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="d-grid">
                                    <button type="submit" id="submit" name="send" class="btn btn-org">{{__('translate.Đăng ký')}}</button>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </form>
                </div>
                <!--end custom-form-->
            </div>
            <!--end col-->

            <div class="col-lg-7 col-md-6 order-1 order-md-2">
                <div class="title-heading ms-lg-4">
                    <h4 class="mb-4">{{__('translate.Thông tin liên hệ')}}</h4>
                    <span class="text-primary fw-bold">OURANSOFT TECHNOLOGY JOINT STOCK COMPANY</span>
                    <div class="d-flex contact-detail align-items-center mt-3">
                        <div class="icon">
                            <i data-feather="mail" class="fea icon-m-md text-dark me-3"></i>
                        </div>
                        <div class="flex-1 content">
                            <h6 class="title fw-bold mb-0">{{__('translate.Email')}}</h6>
                            <span class="text-primary">Ouransoft@gmail.com</span>
                        </div>
                    </div>

                    <div class="d-flex contact-detail align-items-center mt-3">
                        <div class="icon">
                            <i data-feather="phone" class="fea icon-m-md text-dark me-3"></i>
                        </div>
                        <div class="flex-1 content">
                            <h6 class="title fw-bold mb-0">{{__('translate.Điện thoại')}}</h6>
                            <span class="text-primary">0833-993-966</span>
                        </div>
                    </div>

                    <div class="d-flex contact-detail align-items-center mt-3">
                        <div class="icon">
                            <i data-feather="map-pin" class="fea icon-m-md text-dark me-3"></i>
                        </div>
                        <div class="flex-1 content">
                            <h6 class="title fw-bold mb-0">{{__('translate.Địa chỉ')}}</h6>
                            <a href="https://g.page/ouransoft?share" class="video-play-icon text-primary lightbox" target="__blank">{{__('translate.Số 3, lô 16 Mở rộng, Trung Hành 5 Đằng Lâm, Hải An, Hải Phòng')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- End contact -->

<script>
    $("#contact__insert").submit(function (e){
            e.preventDefault();
            form = $(this).serialize();
            url = "{!!route('contact.insert')!!}";
            $.ajax({
                url:url,
                type:"POST",
                data:form,
                success:function(data){
                    if(data.success == 'false'){
                        alert('{{__('translate.Bạn chưa điền đầy đủ thông tin')}}!');
                    }else{
                        alert('{{__('translate.Đăng ký thành công')}}!');
                        $("#contact__insert")[0].reset();
                    }
                }
            });

        });
</script>
