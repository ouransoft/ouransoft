<section class="section pt-0 bg-light">
    <div class="container">
        <div class="row mb-md-5 pt-md-3 mb-4 pt-2 pb-lg-4 justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title">
                    <h4 class="title mb-4">{{trans('translate.Nhận thông tin')}}</h4>
                    <p class="text-muted para-desc mx-auto"><span class="text-org fw-bold">{{trans('translate.Đăng ký nhận tin tức và được tư vấn')}}.</span></p>
                    <div class="d-flex flex-row justify-content-center">
                        <input id="input_sub" type="tel" name="email" class="form-control w-75" placeholder="{{trans('translate.Nhập Email hoặc số điện thoại của bạn')}}..." style="margin-right: 10px;">
                        <button type="submit" class="btn btn-org" id="button_sub"><i class="uil uil-fast-mail"></i></button>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
<script>
    $('#button_sub').click(function(){
        var email = $('#input_sub').val();
        $.ajax({
            url:'/api/add_sub',
            method:'post',
            data:{
                email: email
            },
            success:function(data){
                if(data.success == 'True')
                {
                    alert ('Đăng ký thành công');
                }else{
                    alert ('Bạn chưa điền Email');
                }
            }
        });
    });
</script>
