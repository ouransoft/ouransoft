<!-- Hero Start -->
<section class="bg-auth-home d-table w-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-md-7">
                <div class="title-heading me-lg-4">
                    <h1 class="heading mb-3 text-org">{{$web->description}}</h1>
                    <p class="para-desc"><b>{{trans('translate.'.$web->sub_description)}}</b></p>
                    <div class="mt-4">
                        <a href="{{route('contact')}}" class="btn btn-org mt-2 me-2"><i class="uil uil-envelope"></i> {{trans('translate.Liên hệ với chúng tôi')}}</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <img src="{{asset('images/illustrator/Startup_SVG.png')}}" alt="ảnh" style="width: 100%">
            </div>
        </div>
    </div>
</section>
<!--end section-->
