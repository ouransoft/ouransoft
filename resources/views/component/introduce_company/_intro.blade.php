<section class="bg-half bg-light d-table w-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title">{{__('translate.Giới thiệu')}}</h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="/">{{__('translate.Trang chủ')}}</a></li>
                                <li class="breadcrumb-item text-org active" aria-current="page">{{__('translate.Giới thiệu')}}</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- Start -->
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <div class="row align-items-center">
                    <div class="col-lg-12 mt-4 mt-lg-0 pt-2 pt-lg-0">
                        <div class="card work-container work-modern overflow-hidden rounded border-0 shadow-md">
                            <div class="card-body p-0" style="max-height: 300px">
                                <img src="{{asset('upload/ceo.jpg')}}" class="img-fluid" alt="work-image">
                                <div class="overlay-work bg-dark"></div>
                                <div class="content">
                                    <span class="title text-white d-block fw-bold">Đào Hải</span>
                                    <small class="text-light">C.E.O</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end col-->

                    <!--end col-->
                </div>
                <!--end row-->
            </div>
            <!--end col-->

            <div class="col-lg-7 col-md-6 mt-4 mt-lg-0 pt- pt-lg-0">
                <div class="ms-lg-4">
                    <div class="section-title">
                        <h4 class="title mb-4">{{__('translate.Thông điệp của tổng giám đốc')}}</h4>
                        <p class="text-muted para-desc mb-0">{{__('translate.Thư ngỏ')}}</p>
                        <p class="text-muted para-desc mb-0">
                            &nbsp;&nbsp;&nbsp;&nbsp;{{__('translate.Kính thưa quý khách hàng, quý đối tác và các nhà đầu tư. Thay mặt Công ty Cổ phần công nghệ OURANSOFT tôi xin gửi lời chào, lời chúc sức khoẻ, chúc cho sự hợp tác của chúng ta thành công tốt đẹp')}}.
                        </p>
                        <p class="text-muted para-desc mb-0">
                            &nbsp;&nbsp;&nbsp;&nbsp;{{__('translate.Hoà chung trong sự phát triển của xã hội, sự hội nhập sâu và rộng của nền kinh tế Việt Nam với các nền kinh tế trong khu vực và trên thế giới. Trước xu thế đó, OURANSOFT ra đời nhằm cung cấp tới các quý khách hàng các giải pháp công nghệ, các công cụ quản trị giúp doanh nghiệp của quý khách hàng nâng cao năng lực quản lý nội bộ, tăng cường khả năng thích ứng linh hoạt với môi trường kinh doanh mới. Từ đó, chúng tôi tin sẽ là trợ thủ đắc lực trên con đường thành công bền vững của quý khách hàng, quý đối tác và các nhà đầu tư')}}.
                        </p>
                        <p class="text-muted para-desc mb-0">
                            &nbsp;&nbsp;&nbsp;&nbsp;{{__('translate.Ở OURANSOFT, toàn bộ cán bộ công nhân viên luôn hiểu rằng phải cố gắng, nỗ lực hết mình để đưa ra các giải pháp công nghệ, các công cụ quản trị “ ĐƠN GIẢN HƠN, THÔNG MINH HƠN và HIỆU QUẢ HƠN” bao giờ hết')}}.
                        </p>
                        <p class="text-muted para-desc mb-0">
                            {{__('translate.Để làm được điều đó tại OURANSOFT chúng tôi luôn')}}:
                        </p>
                        <p class="text-muted para-desc mb-0">
                            &nbsp;&nbsp;&nbsp;&nbsp;- {{__('translate.Lắng nghe, thấu hiểu và đồng cảm với những nhu cầu của quý khách hàng, từ đó cung cấp chính xác giải pháp công nghệ, các công cụ quản trị đơn giản, thông minh và hiệu quả nhất tới tay khách hàng vào đúng thời điểm')}}.
                        </p>
                        <p class="text-muted para-desc mb-0">
                            &nbsp;&nbsp;&nbsp;&nbsp;- {{__('translate.Tạo môi trường phát triển bản thân và nuôi dưỡng những ước mơ cho đồng nghiệp')}}.
                        </p>
                        <p class="text-muted para-desc mb-0">
                            &nbsp;&nbsp;&nbsp;&nbsp;- {{__('translate.Nâng cao giá trị gia tăng nhiều hơn cho các nhà đầu tư và các đối tác')}}.
                        </p>
                        <p class="text-muted para-desc mb-0">
                            {{__('translate.Và chúng tôi mong muốn cùng với quý vị xây dựng một xã hội phát triển, phồn vinh và hạnh phúc')}}.
                        </p>
                        <p class="text-muted para-desc mb-0">
                            {{__('translate.Cuối cùng tôi xin cầu chúc sự bình an, thịnh vượng và viên mãn tới gia đình của quý vị')}}.
                        </p>
                        <p class="text-muted mb-0 text-right">
                            {{__('translate.Chủ tịch hội đồng quản trị')}}
                        </p>
                        <p class="text-muted mb-0 text-right pr-5">
                            Đào Hải
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- End -->

<!-- Start -->
<section class="section pt-0 bg-light">
    <div class="container mt-60">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mt-3 mb-4">{{__('translate.Ý tưởng thương hiệu')}} OURANSOFT</h4>
                    <p class="text-muted para-desc mb-0 mx-auto"><span class="text-org fw-bold">OURANSOFT</span>
                        {{__('translate.là thương hiệu được ghép bởi 2 yếu tố')}} <span class="text-org fw-bold">OURAN</span>
                        {{__('translate.và')}}
                        <span class="text-org fw-bold">SOFT</span>
                    </p>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->

        <div class="row">
            <div class="col-md-6 col-12 mt-5">
                <div class="features text-center">
                    <div class="image position-relative d-inline-block">
                        <span class="fw-bold">OURANUS</span>
                    </div>
                    <div class="content mt-4">
                        <p class="text-muted mb-0">{{__('translate.Theo tiếng Hy lạp nghĩa là bầu trời. Bầu trời vốn là một khoảng không bao la rộng lớn, không điểm dừng. Nơi giúp chúng ta tiếp thêm rất nhiều động lực. Cứ mỗi khi cảm thấy trong lòng mỏi mệt, chỉ cần nhìn lên bầu trời xanh thăm thẳm phía trên, bất giác sẽ cảm thấy mọi thứ như nhẹ đi một chút')}}.</p>
                    </div>
                </div>
            </div>
            <!--end col-->

            <div class="col-md-6 col-12 mt-5">
                <div class="features text-center">
                    <div class="image position-relative d-inline-block black-feature">
                        <span class="fw-bold">SOFT</span>
                    </div>
                    <div class="content mt-4">
                        <p class="text-muted mb-0">{{__('translate.Có nghĩa là mềm mại, mềm mỏng. Các giải pháp công nghệ và các công cụ quản trị OURANSOFT xây dựng cũng dựa trên nguyên tắc giúp mọi người giải quyết công việc một cách đơn giản hơn, thông minh hơn và hiệu quả hơn từ đó giúp mọi người có một cuộc sống thành công, viên mãn hơn. Những nguyên tắc này cũng đại diện cho niềm tin của OURANSOFT nói chung')}}.
                        </p>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->

</section>
<!--end section-->
<!-- End section -->

<!-- Start -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6 bg-light rounded">
                <div class="section-title">
                    <div class="p-4">
                        <p class="text-org h6 fst-italic">“...{{__('translate.Bạn cần một người đồng hành, hãy chia sẻ những gì mình có')}}</p>
                        <p class="text-muted h6 fst-italic">&nbsp;&nbsp;&nbsp;&nbsp;{{__('translate.Chúng tôi luôn tâm niệm, để trở thành người đồng hành cùng với các khách hàng, các đối tác, các nhà đầu tư và với toàn bộ cán bộ công nhân, dù vấn đề lớn hay nhỏ chúng tôi sẽ luôn lắng nghe và chia sẻ những gì mà mình có, mình biết nhằm giúp khách hàng, đối tác nhà đầu tư và cán bộ công nhân viên giải quyết vấn đề đó một cách tốt nhất')}}.”</p>
                        <img src="{{asset('upload/dao-hai.jpg')}}" class="img-fluid avatar avatar-small rounded-circle mx-auto shadow" alt="">
                        <ul class="list-unstyled mb-0 mt-3">
                            <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                            <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                            <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                            <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                            <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                        </ul>
                        <h6 class="text-org">- Đào Hải <small class="text-muted">C.E.O</small></h6>
                    </div>
                </div>
            </div><!--end col-->
            <div class="col-lg-6 col-md-6">
                <img src="images/blog/05.jpg" class="img-fluid rounded" alt="">
            </div><!--end col-->
        </div><!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- End -->

