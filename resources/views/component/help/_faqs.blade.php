<section class="bg-half bg-light d-table w-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title">Các câu hỏi thường gặp</h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                                <li class="breadcrumb-item text-org active" aria-current="page">FAQs</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- Start Section -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-5 col-12 d-none d-md-block">
                <div class="rounded shadow p-4 sticky-bar">
                    <ul class="list-unstyled sidebar-nav mb-0 py-0" id="navmenu-nav">
                        <li class="navbar-item my-2 px-0"><a href="#general" class="h6 text-dark navbar-link">Câu hỏi chung</a></li>
                        <li class="navbar-item my-2 px-0"><a href="#support" class="h6 text-dark navbar-link">Câu hỏi hỗ trợ</a></li>
                    </ul>
                </div>
            </div><!--end col-->

            <div class="col-lg-8 col-md-7 col-12">

                <div class="section-title" id="general">
                    <h4>Câu hỏi chung</h4>
                </div>

                <div class="accordion mt-4 pt-2" id="generalquestion">
                    <div class="accordion-item rounded">
                        <h2 class="accordion-header" id="headingfive">
                            <button class="accordion-button border-0 bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefive"
                                aria-expanded="true" aria-controls="collapsefive">
                                How does it work ?
                            </button>
                        </h2>
                        <div id="collapsefive" class="accordion-collapse border-0 collapse show" aria-labelledby="headingfive"
                            data-bs-parent="#generalquestion">
                            <div class="accordion-body text-muted bg-white">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item rounded mt-2">
                        <h2 class="accordion-header" id="headingsix">
                            <button class="accordion-button border-0 bg-light collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsesix"
                                aria-expanded="false" aria-controls="collapsesix">
                                Do I need a designer to use Landrick ?
                            </button>
                        </h2>
                        <div id="collapsesix" class="accordion-collapse border-0 collapse" aria-labelledby="headingsix"
                            data-bs-parent="#generalquestion">
                            <div class="accordion-body text-muted bg-white">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item rounded mt-2">
                        <h2 class="accordion-header" id="headingseven">
                            <button class="accordion-button border-0 bg-light collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
                                What do I need to do to start selling ?
                            </button>
                        </h2>
                        <div id="collapseseven" class="accordion-collapse border-0 collapse" aria-labelledby="headingseven"
                            data-bs-parent="#generalquestion">
                            <div class="accordion-body text-muted bg-white">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item rounded mt-2">
                        <h2 class="accordion-header" id="headingeight">
                            <button class="accordion-button border-0 bg-light collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
                                What happens when I receive an order ?
                            </button>
                        </h2>
                        <div id="collapseeight" class="accordion-collapse border-0 collapse" aria-labelledby="headingeight"
                            data-bs-parent="#generalquestion">
                            <div class="accordion-body text-muted bg-white">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section-title mt-5" id="support">
                    <h4>Câu hỏi hỗ trợ</h4>
                </div>

                <div class="accordion mt-4 pt-2" id="supportquestion">
                    <div class="accordion-item rounded">
                        <h2 class="accordion-header" id="headingthirteen">
                            <button class="accordion-button border-0 bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#collapsethirteen"
                                aria-expanded="true" aria-controls="collapsethirteen">
                                How does it work ?
                            </button>
                        </h2>
                        <div id="collapsethirteen" class="accordion-collapse border-0 collapse show" aria-labelledby="headingthirteen"
                            data-bs-parent="#supportquestion">
                            <div class="accordion-body text-muted bg-white">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item rounded mt-2">
                        <h2 class="accordion-header" id="headingfourteen">
                            <button class="accordion-button border-0 bg-light collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefourteen"
                                aria-expanded="false" aria-controls="collapsefourteen">
                                Do I need a designer to use Landrick ?
                            </button>
                        </h2>
                        <div id="collapsefourteen" class="accordion-collapse border-0 collapse" aria-labelledby="headingfourteen"
                            data-bs-parent="#supportquestion">
                            <div class="accordion-body text-muted bg-white">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item rounded mt-2">
                        <h2 class="accordion-header" id="headingfifteen">
                            <button class="accordion-button border-0 bg-light collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapsefifteen" aria-expanded="false" aria-controls="collapsefifteen">
                                What do I need to do to start selling ?
                            </button>
                        </h2>
                        <div id="collapsefifteen" class="accordion-collapse border-0 collapse" aria-labelledby="headingfifteen"
                            data-bs-parent="#supportquestion">
                            <div class="accordion-body text-muted bg-white">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item rounded mt-2">
                        <h2 class="accordion-header" id="headingsixteen">
                            <button class="accordion-button border-0 bg-light collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapsesixteen" aria-expanded="false" aria-controls="collapsesixteen">
                                What happens when I receive an order ?
                            </button>
                        </h2>
                        <div id="collapsesixteen" class="accordion-collapse border-0 collapse" aria-labelledby="headingsixteen"
                            data-bs-parent="#supportquestion">
                            <div class="accordion-body text-muted bg-white">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->

    <div class="container mt-100 mt-60">
        <div class="row">
            <div class="row pt-md-3 pt-2 pb-lg-4 justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title">
                        <h4 class="title mb-4">Nhận thông tin</h4>
                        <p class="text-muted para-desc mx-auto"><span class="text-org fw-bold">Đăng ký nhận tin tức và được tư vấn.</span></p>
                        <div class="d-flex flex-row justify-content-center">
                            <input id="input_sub" type="tel" name="email" class="form-control w-75" placeholder="Nhập Email hoặc số điện thoại của bạn..." style="margin-right: 10px;">
                            <button type="submit" class="btn btn-org" id="button_sub"><i class="uil uil-fast-mail"></i></button>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- End Section -->
<script>
    $('#button_sub').click(function(){
        var email = $('#input_sub').val();
        $.ajax({
            url:'/api/add_sub',
            method:'post',
            data:{
                email: email
            },
            success:function(data){
                if(data.success == 'True')
                {
                    alert ('Đăng ký thành công');
                }else{
                    alert ('Bạn chưa điền Email');
                }
            }
        });
    });
</script>
