<!-- Footer Start -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                <div class="col-12 d-flex justify-content-center">
                    <img src="{{asset('upload/logo-ouransoft-foot.png')}}" class="w-25 h-25" alt="logo">
                </div>
                <h4 class="mt-4 text-center">Ouransoft Technology JSC</h4>
            </div><!--end col-->

            <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-light footer-head">{{trans('translate.Thông tin liên hệ')}}</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li class="text-foot"><i class="uil uil-location-point me-1"></i> {{__('translate.'.$web->address)}}</li>
                    <li class="text-foot"><i class="uil uil-phone me-1"></i> {{$web->tel}}</li>
                    <li class="text-foot"><i class="mdi mdi-email me-1"></i> contact@ouransoft.vn</li>
                    <li class="text-foot"><i class="uil uil-google me-1"></i> www.ouransoft.vn</li>
                </ul>
            </div><!--end col-->

            <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-light footer-head">{{trans('translate.Sản phẩm ứng dụng')}}</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li class="text-foot"><i class="uil uil-apps me-1"></i> i-VIBO</li>
                    <li class="text-foot"><i class="uil uil-apps me-1"></i> Madocar</li>
                    <li class="text-foot"><i class="uil uil-apps me-1"></i> Insights me</li>
                </ul>
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-light footer-head">{{trans('translate.Khác')}}</h5>
                <ul class="list-unstyled social-icon foot-social-icon mb-0 mt-4">
                    <li class="list-inline-item mr-3"><a href="{{$web->facebook}}" class="rounded" style="background: #385898"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item mr-3"><a href="{{$web->instagram}}" class="rounded" style="background: #c12966"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="{{$web->twitter}}" class="rounded" style="background: #34b3f7"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                </ul><!--end icon-->
                {{-- <img src="{{asset('upload/da-thong-bao-bo-cong-thuong.png')}}" class="w-75" alt="logo"> --}}
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</footer><!--end footer-->
