@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tin tức</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tin tức</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Viết bài
              </h3>
            </div>
            <!-- left column -->
            <form action="{{url('admin/news/update')}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <input type="hidden" name="news-id" value="{{$records->id}}">
              <div class="col-md-6">
                <!-- general form elements -->
                <div class="card card-primary">
                  <!-- form start -->
                  <div class="card-body">
                    <div class="form-group">
                      <label for="news-title">Tiêu đề</label>
                      <input type="text" name="news-title" value="{{$records->title}}" class="form-control" id="news-title" placeholder="Nhập tiêu đề">
                    </div>
                    <div class="form-group">
                      <label for="news-category">Danh mục</label>
                      <input type="text" name="news-category" value="{{$records->category}}" class="form-control" id="news-category" placeholder="Nhập danh mục">
                    </div>
                    <div class="form-group">
                      <label for="news-description">Mô tả</label>
                      <input type="text" name="news-description" value="{{$records->description}}" class="form-control" id="news-description" placeholder="Nhập mô tả">
                    </div>
                    <div class="form-group">
                      <label for="news-ordering">Sắp xếp</label>
                      <input type="text" name="news-ordering" value="{{$records->ordering}}" class="form-control" id="news-ordering" placeholder="Nhập thứ tự hiển thị">
                    </div>
                    <div class="form-group">
                      <label for="news-meta-title">Meta title</label>
                      <input type="text" name="news-meta-title" value="{{$records->metatitle}}" class="form-control" id="news-meta-title">
                    </div>
                    <div class="form-group">
                      <label for="news-meta-description">Meta description</label>
                      <input type="text" name="news-meta-description" value="{{$records->metadescription}}" class="form-control" id="news-meta-description">
                    </div>
                    <div class="form-group">
                      <label for="news-meta-keyword">Meta keyword</label>
                      <input type="text" name="news-meta-keyword" value="{{$records->metakeyword}}" class="form-control" id="news-meta-keyword">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Hình ảnh</label>
                      <br>
                          <div class="img-main" style="border: 2px dashed #0087F7; border-radius: 5px;">
                            <img id="blah" src="#" style="width:20%;" />
                          </div>
                          <label for="news-image" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file
                            <input type="file" name="news-image" id="news-image" accept="image/*" class="news-image" multiple hidden/>
                          </label>
                    </div>
                    <div class="form-group">
                      <label>Hẹn ngày giờ đăng:</label>
                      <!-- Date and time picker with disbaled dates -->
                      <div class="form-group">
                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                            <input type="date" name="timer" class="form-control datetimepicker-input" data-target="#reservationdate" value="{{$records->timer}}"/>
                        </div>
                        <!-- /.input group -->
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <div class="card-body">
                <textarea id="editor" name="news-content">
                    {!!$records->content!!}
                </textarea>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
  <script>
    CKEDITOR.replace( 'editor', {
        filebrowserUploadUrl: "{{route('admin.ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
    $("#news-image").change(function() {
      readURL(this);
    });
  </script>
  <style>
     [hidden] {
      display: none !important;
    }
  </style>
@endsection
