@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tin tức</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tin tức</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Viết bài
              </h3>
            </div>
            <!-- left column -->
            <form action="{{url('admin/news/store')}}" method="post" enctype="multipart/form-data" runat="server">
              {{csrf_field()}}
              <div class="col-md-6">
                <!-- general form elements -->
                <div class="card card-primary">
                  <!-- form start -->
                  <div class="card-body">
                    <div class="form-group">
                      <label for="news-title">Tiêu đề</label>
                      <input type="text" name="news-title" class="form-control" id="news-title" value="{{old('news-title')}}" placeholder="Nhập tiêu đề">
                      @error('news-title')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="news-category">Danh mục</label>
                      <input type="text" name="news-category" class="form-control" id="news-category" value="{{old('news-category')}}" placeholder="Nhập danh mục">
                      @error('news-category')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="news-description">Mô tả</label>
                      <input type="text" name="news-description" class="form-control" id="news-description" value="{{old('news-description')}}" placeholder="Nhập mô tả">
                      @error('news-description')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="news-ordering">Sắp xếp</label>
                      <input type="text" name="news-ordering" class="form-control" id="news-ordering" value="{{old('news-ordering')}}" placeholder="Nhập thứ tự hiển thị">
                      @error('news-ordering')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="news-meta-title">Meta title</label>
                      <input type="text" name="news-meta-title" class="form-control" id="news-meta-title" value="{{old('news-meta-title')}}">
                      @error('news-meta-title')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="news-meta-description">Meta description</label>
                      <input type="text" name="news-meta-description" class="form-control" id="news-meta-description" value="{{old('news-meta-description')}}">
                      @error('news-meta-description')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="news-meta-keyword">Meta keyword</label>
                      <input type="text" name="news-meta-keyword" class="form-control" id="news-meta-keyword" value="{{old('news-key-word')}}">
                      @error('news-meta-keyword')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Hình ảnh</label>
                      <br>
                      <div class="for-download" style=" border: 2px dashed #0087F7; border-radius: 5px; width:100%" >
                        <div class="img-main " >
                          <img id="blah"  style=" width:20%;" /> 
                        </div>
                      </div>
                      <label for="news-image" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file   
                        <input type="file" name="news-image" accept="image/*" id="news-image" class="news-image" multiple hidden/>   
                      </label> 
                      @error('news-image')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Hẹn ngày giờ đăng:</label>
                      <!-- Date and time picker with disbaled dates -->
                      <div class="form-group">
                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                            <input type="date" name="timer" class="form-control datetimepicker-input" data-target="#reservationdate"  value="{{old('timer')}}"/>
                        </div>
                        <!-- /.input group -->
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <div class="card-body">
                <textarea id="editor" name="news-content"></textarea>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
  <script>
    CKEDITOR.replace( 'editor', {
        filebrowserUploadUrl: "{{route('admin.news.insert', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
    $("#news-image").change(function() {
      readURL(this);
    });
  </script>
  <style>
    [hidden] {
      display: none !important;
    }

  </style>
@endsection
