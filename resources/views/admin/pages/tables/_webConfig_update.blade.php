@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cấu hình web</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Cấu hình web</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
          <form action=" {{ url('/admin/webconfig/update')}} " method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card-body">
              <div class="form-group">
                <label for="web-name">Tên trang</label>
                <input type="text" name="name" class="form-control" id="web-name" placeholder="Nhập tên trang" value="{{ $records->name }}">
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Logo</label>
                <br>
                  <div class="for-download" style=" border: 2px dashed #0087F7; border-radius: 5px; width:100%" >
                    <div class="img-main " >
                      <img class="img-display" /> 
                    </div>
                  </div>
                  <label for="web-logo" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file  
                    <input type="file" name="logo" id="web-logo" accept="image/*" class="web-logo" multiple hidden/>   
                  </label> 
              </div>
              <div class="form-group1">
                <label for="exampleInputFile1">Favicon</label>
                <br>
                  <div class="for-download1" style=" border: 2px dashed #0087F7; border-radius: 5px; width:100%" >
                    <div class="img-main1 " >
                      <img class="img-display1" /> 
                    </div>
                  </div>
                  <label for="web-favicon" class="btn btn-info form-control alt1"><i class="fas fa-upload"></i>Choose a file   
                    <input type="file" name="favicon" id="web-favicon" accept="image/*" class="web-favicon" multiple hidden/>   
                  </label> 
              </div>
              <div class="form-group">
                <label for="web-address">Địa chỉ</label>
                <input type="text" name="address" class="form-control" id="web-address" placeholder="Nhập địa chỉ" value="{{ $records->address }}">
              </div>
              <div class="form-group">
                <label for="web-tel">Số điện thoại</label>
                <input type="text" name="tel" class="form-control" id="web-tel" placeholder="Nhập số điện thoại" value="{{ $records->tel }}">
              </div>
              <div class="form-group">
                <label for="web-description">Mô tả</label>
                <input type="text" name="description" class="form-control" id="web-description" placeholder="Nhập nội dung" value="{{ $records->description }}">
              </div>
              <div class="form-group">
                <label for="web-subdescription">Mô tả phụ</label>
                <input type="text" name="sub_description" class="form-control" id="web-description" placeholder="Nhập nội dung" value="{{ $records->sub_description }}">
              </div>
              <div class="form-group">
                <label for="web-facebook">facebook</label>
                <input type="text" name="facebook" class="form-control" id="web-facebook" value="{{ $records->facebook }}">
              </div>
              <div class="form-group">
                <label for="web-twitter">Twitter</label>
                <input type="text" name="twitter" class="form-control" id="web-twitter" value="{{ $records->twitter }}">
              </div>
              <div class="form-group">
                <label for="web-instagram">Instagram</label>
                <input type="text" name="instagram" class="form-control" id="web-instagram" value="{{ $records->instagram }}">
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Cập nhật</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
<script>
  // function readURL(input) {
  //   if (input.files && input.files[0]) {
  //     var reader = new FileReader();
  //     reader.onload = function(e) {
  //       $('#blah').attr('src', e.target.result);
  //     }
  //     reader.readAsDataURL(input.files[0]); // convert to base64 string
  //   }
  // }
  // $("#web-logo").change(function() {
  //   readURL(this);
  // });
  //
  $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img  class="img-display" style=" width:10%; padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#web-logo').change(function(){
        imagesPreview(this,'div.img-main');
    });
});
///////////
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img  class="img-display1" style=" width:10%; padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#web-favicon').change(function(){
        imagesPreview(this,'div.img-main1');
    });
});
</script>
<style>
  [hidden] {
    display: none !important;
  }

</style>
@endsection