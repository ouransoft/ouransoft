@extends('admin/index3')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lý sản phẩm</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lý sản phẩm</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">

              <form action="{{url('admin/product/insert')}}" method="POST" enctype="multipart/form-data"   >
                {{csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="product-name">Tên Sản phẩm</label>
                    <input type="text" name="name" class="form-control mb-2" id="product-name1" placeholder="Nhập tên sản phẩm">
                    @error('name')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="product-content">Nội dung</label>
                    <input type="text" name="content" class="form-control mb-2"  style="word-break: break-all"id="product-content1" placeholder="Nhập nội dung">
                    @error('content')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="product-description">Mô tả</label>
                    <input type="text" name="description" class="form-control mb-2" id="product-description1" placeholder="Nhập mô tả">
                    @error('description')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="product-link">Link</label>
                    <input type="text" name="link" class="form-control mb-2" placeholder="Nhập link">
                    {{-- @error('link')
                      <div class="alert alert-danger">{{ $link }}</div>
                    @enderror --}}
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Hình ảnh</label>
                    <br>
                    <div class="img-main " style="    border: 2px dashed #0087F7; border-radius: 5px; width:100% ;">
                      <img class="img-display" >
                    </div>
                    <label for="product-image" class="btn btn-info form-control" ><i class="fas fa-upload"></i>Choose a file
                      <input type='file' id="product-image" name="image"  accept="image/*" multiple hidden/>
                    </label>
                    @error('image')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
              </form>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr style="text-align: center;">
                          <th>Tên sản phẩm</th>
                          <th>Hình ảnh</th>
                          <th>Nội dung</th>
                          <th>Link sản phẩm</th>
                          <th>Mô tả</th>
                          <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($records as $records)
                        <tr id="product-detail{{$records->id}}">
                            <td style="text-align: center; width:15%">{{$records->name}}</td>
                            <td style="text-align: center; width:10%"><img src="/upload/product/{{$records->image}}" alt="1" width="50%"></td>
                            <td style="text-align: center; width:20%">{{$records->content}}</td>
                            <td style="text-align: center; width:20%">{{$records->link}}</td>
                            <td style="text-align: center; width:20%">{{$records->description}}</td>
                            <td style="text-align: center; width:20%">
                              <a class="btn btn-info" href="{{url('admin/product/edit/'.$records->id)}}"><i class="fas fa-pencil-alt"></i></a>
                              <a data-id="{{$records->id}}" class="Product-delete btn btn-danger" href=""><i class="fas fa-times-circle"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- Modal -->
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
   //delete-product
    $('.Product-delete').click(function(){
        const id = $(this).data('id');
        var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
        if(cfrm == true){
        $.ajax({
              url: "/admin/product/delete/"+id,
              method: "get",
              success:function() {
                console.log('#product-detail'+id)
                $('#product-detail'+id).remove();
              }
        });
      }
    });
    /////
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img  class="img-display" style=" width:10%; padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#product-image').change(function(){
        imagesPreview(this,'div.img-main');
    });
});

 </script>
  <style>
    [hidden] {
     display: none !important;
   }
 </style>

@endsection
