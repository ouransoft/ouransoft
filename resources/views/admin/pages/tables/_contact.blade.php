@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lý thông tin liên hệ</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lý thông tin liên hệ</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="menu-table" class="table table-bordered table-striped">
                    <thead style="text-align: center">
                        <tr>
                        <th>Họ và tên</th>
                        <th>Email</th>
                        <th>Nội dung</th>
                        <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($contact as $contact)
                        <tr>
                            <td style="text-align: center; width:25% "  >{{($contact->name)}}</td>
                            <td style="text-align: center; width:25%" >{{($contact->email)}}</td>
                            <td style="text-align: center; width:25%" >{{($contact->content)}}</td>
                            <td style="text-align: center; width:25%" ><a data-id="{{ $contact->id }}" href="" class="Contact-delete menu-delete btn btn-danger"><i class="fas fa-times-circle"></i></a></td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
                <script>
                  $('.Contact-delete').click(function(){
                          const id = $(this).data('id');
                          var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
                          if(cfrm == true){
                          $.ajax({
                                url: "/admin/contact/delete/"+id,
                                method: "get",
                          });
                        }
                  });
                </script>
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
