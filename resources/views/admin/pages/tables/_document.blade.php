@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lý tài liệu</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lý tài liệu</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">
              <form action="{{ url('admin/document/insert')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="document-title1">Tiêu đề</label>
                    <input type="text" name="document-title" class="form-control mb-2" id="document-title1" placeholder="Nhập tiêu đề">
                    @error('document-title')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="document-ordering1">Sắp xếp</label>
                    <input type="text" name="document-ordering" class="form-control mb-2" id="document-ordering1" placeholder="Nhập thứ tự hiển thị">
                    @error('document-ordering')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="document-image1">Hình ảnh</label>
                    <br>
                        <div class="img-main "  style=" border: 2px dashed #0087F7; border-radius: 5px; ">
                          <img class="img-display">
                        </div>
                        <label for="document-image" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file
                            <input type="file" name="document-image" id="document-image" accept="image/*" class="document-image" multiple hidden/>
                        </label>

                        @error('document-image')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="document-image1">Files</label>
                    <br>
                    <div class="file-main">
                        <label class="file1 form-control" for="document-file" accept=".doc,.txt,.pdf,.sql,.xlsx" id="bleh"><i class="far fa-file"></i></label>
                    </div>
                    <label for="document-file" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file
                          <input  type="file"  name="files[]" id="document-file"  class="form-control mb-2" multiple hidden/>
                    </label>
                  <label for="document-file1" name="files[]"></label>
                    @error('files')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
              </form>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead style="text-align: center;" >
                        <tr>
                            <th>Tiêu đề</th>
                            <th>Hình ảnh</th>
                            <th>Files</th>
                            <th>Thứ tự sắp xếp</th>
                            <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($records as $record)
                        <tr id="documentid-{{$record->id}}">
                            <td style="text-align: center; width:20%">{{$record->title}}</td>
                            <td style="text-align: center;  width:20%"><img src="/upload/document/image/{{$record->image}}" alt="1" width="80%"></td>
                            <td style="text-align: center;  width:20%">
                                @foreach (explode(',', $record->file) as $rc)
                                    <img src="/upload/document/file/{{$rc}}" alt="1" width="80%">
                                @endforeach
                            </td>
                            <td style="text-align: center;  width:20%">{{$record->ordering}}</td>
                            <td style="text-align: center;  width:20%">
                              <a class="btn btn-info" href="{{url('admin/document/edit/'.$record->id)}}"><i class="fas fa-pencil-alt"></i></a>
                              <a class="btn btn-danger document-delete" data-id="{{$record->id}}"><i class="fas fa-times-circle"></i></a>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
              </div>
              <!-- Modal -->
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
  $('.document-delete').click(function(){
      const id = $(this).data('id');
      var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
      if(cfrm == true){
        $.ajax({
          url: "/admin/document/delete/"+id,
          method: "get",
          data: {
            'id' : id,
          },
          success: function(res){
            $("#documentid-"+id).remove();
          }
        });
      }
    });
    $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img  class="img-display" style=" width:10%; padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#document-image').change(function(){
        imagesPreview(this,'div.img-main');
    });
});
///
    function readFile(input) {
      if (input.files ) {
         var arr = []
          jQuery.each(input.files, function( i, val ) {
              arr.push(val.name);
            });
            x=arr.join(" ,");
            $('.file1').append(x);

      }
    }
    $("#document-file").change(function() {
      readFile(this);
    });
</script>
<style>
  [hidden] {
    display: none !important;
  }

</style>
@endsection
