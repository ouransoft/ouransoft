@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lý tin tức</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lý tin tức</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">
              <!-- /.card-header -->
              <a href="{{url('/admin/news/insert')}}" class="btn btn-primary">Thêm bài viết mới</a>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead style="text-align: center">
                      <tr>
                        <th>Tiêu đề</th>
                        <th>Danh mục</th>
                        <th>Hình ảnh</th>
                        <th>Tác vụ</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($records as $record)
                      <tr id="newsid-{{$record->id}}">
                          <td style="text-align: center; width:25%">{{$record->title}}</td>
                          <td style="text-align: center; width:25%">{{$record->category}}</td>
                          <td style="text-align: center; width:25%"><img src="/upload/news/image/{{$record->image}}" alt="ảnh" style="width: 100%"></td>
                          <td style="text-align: center; width:25%">
                            <a href="{{ url('admin/news/edit/'.$record->id) }}" class="btn btn-info"><i class="fas fa-pencil-alt"></i></a>
                            <a class="btn btn-danger news-delete" data-id="{{$record->id}}"><i class="fas fa-times-circle"></i></a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
  $('.news-delete').click(function(){
    const id = $(this).data('id');
    var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
    if(cfrm == true){
      $.ajax({
        url: "/admin/news/delete/"+id,
        method: "get",
        data: {
          'id' : id,
        },
        success: function(res){
          $("#newsid-"+id).remove();
        }
      });
    }
  });
</script>
@endsection