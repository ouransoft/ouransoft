@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lý menu</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lý menu</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">
              <form action="{{ url('/admin/menu/insert') }}" method="post">
                {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="menu-title1">Tiêu đề</label>
                    <input type="text" name="title" class="form-control mb-2" id="menu-title1" placeholder="Nhập tiêu đề">
                    @error('title')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="menu-link1">Link</label>
                    <input type="text" name="link" class="form-control mb-2" id="menu-link1" placeholder="Nhập đường dẫn">
                    @error('link')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="menu-ordering1">Sắp xếp</label>
                    <input type="text" name="ordering" class="form-control mb-2" id="menu-ordering1" placeholder="Nhập vị trí">
                    @error('ordering')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
              </form>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="menu-table" class="table table-bordered table-striped">
                    <thead style="text-align: center;">
                        <tr>
                        <th>Tiêu đề</th>
                        <th>Thứ tự sắp xếp</th>
                        <th>Link</th>
                        <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($records as $record)
                        <tr id="menuid-{{$record->id}}">
                            <td style="text-align: center; width:25%">{{$record->title}}</td>
                            <td style="text-align: center; width:25%">{{$record->ordering}}</td>
                            <td style="text-align: center; width:25%">{{$record->link}}</td>
                            <td style="text-align: center; width:25%">
                              <a class="btn btn-info" href="{{url('/admin/menu/edit/'.$record->id)}}"><i class="fas fa-pencil-alt"></i></a>
                              <a data-id="{{$record->id}}" class="menu-delete btn btn-danger"><i class="fas fa-times-circle"></i></a>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
              </div>
              <!-- Modal -->
          
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <script>
  
    $('.menu-delete').click(function(){
      const id = $(this).data('id');
      var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
      if(cfrm == true){
        $.ajax({
          url: "/admin/menu/delete/"+id,
          method: "get",
          data: {
            'id' : id,
          },
          success: function(){
            $("#menuid-"+id).remove();
          }
        });
      }
    });
  </script>
@endsection
