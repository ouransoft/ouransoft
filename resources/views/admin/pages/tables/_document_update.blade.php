@extends('admin/index3')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lí tài liệu</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lí tài liệu</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <form action="{{url('/admin/document/update')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="card-body">
                        <input type="hidden" name="document-id" class="form-control mb-2"  value="{{$document->id}}">
                            <div class="form-group">
                                  <label for="document-title">Tiêu đề</label>
                                    <input type="text" name="document-title" class="form-control" id="document-title" placeholder="Nhập tiêu đề" value="{{$document->title}}">
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>        
                            <div class="form-group">
                                <label for="document-ordering">Sắp xếp</label>
                                    <input type="text" name="document-ordering" class="form-control" id="document-ordering" placeholder="Nhập thứ tự hiển thị" value="{{$document->ordering}}">
                                    @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-group">
                                <label for="document-image">Hình ảnh</label>
                                    <br>  
                                <div class="img-main " style="border: 2px dashed #0087F7; border-radius: 5px;">
                                      <img class="img-display"/> 
                                </div>
                                <label for="document-image" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file 
                                  <input type="file" name="document-image" id="document-image" accept="image/*" class="document-image" multiple hidden/>
                              </label>  
                            </div>
                            <div class="form-group">
                                  <label for="document-file">Files</label>
                                  <br>
                                    <div class="file-main">
                                      <label class="file form-control filealt" for="document-file" accept=".doc,.txt,.pdf,.sql"  name="files[]" id="bleh" value="files"><i class="far fa-file"></i></label> 
                                    </div>
                                    <label for="document-file" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file   
                                      <input type="file" name="files[]" accept=".doc,.txt,.pdf,.sql" id="document-file" class="form-control mb-2"  multiple hidden/>
                                  </label>  

                            </div>
                    </div>
            
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </section>
</div>
<script>
    // function readURL(input) {
    //   if (input.files && input.files[0] ) {
    //     var reader = new FileReader();
    //     reader.onload = function(e) {
    //       $('#blah').attr('src', e.target.result);
    //     }
    //     reader.readAsDataURL(input.files[0]); // convert to base64 string
    //   }
    // }
    // $("#document-image").change(function() {
    //   readURL(this);
    // });
    // ///
    // function readFile(input) {
    //   if (input.files ) {
    //     jQuery.each(input.files,function(i,val){
    //         console.log(val);
    //         $('.filealt').append(val.name);
    //     });
    //   }
    //   }
    // $("#document-file").change(function() {
    //   readFile(this);
    // });
    //
    $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img  class="img-display" style=" width:10%; padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#document-image').change(function(){
        imagesPreview(this,'div.img-main');
    });
});
</script>
<style>
  [hidden] {
    display: none !important;
  }

</style>
  @endsection