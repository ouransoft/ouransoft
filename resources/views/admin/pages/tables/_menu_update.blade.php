@extends('admin/index3')

@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Quản lý menu</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Quản lý menu</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="modal-body">
              <form action="{{url('/admin/menu/update')}}" method="post">
                {{csrf_field()}}
                <div class="card-body">
                  <input type="hidden" id="menu-id" name="menu-id"  value="{{$menu->id}}">
                  <div class="form-group">
                    <label for="menu-title">Tiêu đề</label>
                    <input type="text" name="menu-title" class="form-control" id="menu-title" placeholder="Nhập tiêu đề" value="{{$menu->title}}">
                  </div>
                  <div class="form-group">
                    <label for="menu-link">Link</label>
                    <input type="text" name="menu-link" class="form-control" id="menu-link" placeholder="Nhập đường dẫn" value="{{$menu->link}}">
                  </div>
                  <div class="form-group">
                    <label for="menu-ordering">Sắp xếp</label>
                    <input type="text" name="menu-ordering" class="form-control" id="menu-ordering" placeholder="Nhập vị trí" value="{{$menu->ordering}}">
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
              </form>
        </div>
      </div>
    </div>
  </div>
</section>

    </div>
  @endsection