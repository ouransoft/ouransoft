@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cấu hình web</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Cấu hình web</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" style="text-align: center;">
                    <tr >
                      <th style="text-align: center; " >Tên trang</th>
                      <th style="text-align: center;">Mô tả</th>
                      <th style="text-align: center;">Mô tả phụ</th>
                      <th style="text-align: center;">favicon</th>
                      <th style="text-align: center;">Logo</th>
                      <th style="text-align: center;">Địa chỉ</th>
                      <th style="text-align: center;">Số điện thoại</th>
                      <th style="text-align: center;">Facebook</th>
                      <th style="text-align: center;">Twitter</th>
                      <th style="text-align: center;">Instagram</th>
                      <th style="text-align: center;">Tác vụ</th>
                    </tr>
                    <tr>
                      <td style="text-align: center; ">{{ $records->name }}</td>
                      <td style="text-align: center; ">{{ $records->description }}</td>
                      <td style="text-align: center; ">{{ $records->sub_description }}</td>
                      <td style="text-align: center; "><img src="/upload/webconfig/{{ $records->favicon }}" alt="favicon" width="100%"></td>
                      <td style="text-align: center; "><img src="/upload/webconfig/{{ $records->logo }}" alt="logo" width="100%"></td>
                      <td style="text-align: center; ">{{ $records->address }}</td>
                      <td style="text-align: center; ">{{ $records->tel }}</td>
                      <td style="text-align: center; ">{{ $records->facebook }}</td>
                      <td style="text-align: center; ">{{ $records->twitter }}</td>
                      <td style="text-align: center; ">{{ $records->instagram }}</td>
                      <td style="text-align: center; ">
                        <a href="{{url('/admin/webconfig/edit')}}"><button type="submit" class="btn btn-primary" >
                            <i class="fas fa-pencil-alt"></i>
                          </button>
                        </a>
                      </td>
                    </tr>
                </table>
              </div>

              <!-- Modal -->
             
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
