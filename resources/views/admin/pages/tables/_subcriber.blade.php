@extends('admin/index3')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Thông tin đăng ký</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Home</a></li>
              <li class="breadcrumb-item active">Thông tin đăng ký</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead style=" text-align: center">
                    <tr>
                      <th >Email</th>
                      <th>Ngày tạo</th>
                      <th>Tác vụ</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($records as $record)
                    <tr id="subid-{{$record->id}}">
                      <td style=" text-align: center ; width:40%">{{ $record->email }}</td>
                      <td style=" text-align: center ; width:40%" >{{ $record->created_at }}</td>
                      <td style=" text-align: center ; width:20%"><button data-id="{{$record->id}}" class="sub-delete btn btn-danger"><i class="fas fa-times-circle"></i></button></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
      $('.sub-delete').click(function(){
      const id = $(this).data('id');
      var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
      if(cfrm == true){
        $.ajax({
          url: "/admin/subcriber/delete/"+id,
          method: "get",
          data: {
            'id' : id,
          },
          success: function(){
            $("#subid-"+id).remove();
          }
        });
      }
    });
</script>
@endsection
