@extends('admin/index3')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lí đối tác</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lí đối tác</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <form action="{{url('admin/partner/update')}}" method="post" enctype="multipart/form-data" runat="server">
                {{csrf_field()}}
                <div class="card-body">
                    <input type="hidden" name="partner-id" class="form-control mb-2"  value="{{$partner->id}}">
                        <div class="form-group">
                            <label for="decentralization-name">Tên thành viên</label>
                                <input type="text" name="name" class="form-control mb-2" id="" placeholder="Nhập tên" value="{{$partner->name}}">
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                        </div>        
                        <div class="form-group">
                            <label for="decentralization-email">Link</label>
                                <input type="text" name="link" class="form-control mb-2" placeholder="Nhập Email" value="{{$partner->link}}">
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="decentralization-passd">Hình ảnh</label>
                                <br>
                                <div class="img-main " style="border: 2px dashed #0087F7; border-radius: 5px;">
                                  <img  class="img-display"/> 
                                </div>
                                <label for="partner-img" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file
                                  <input type='file' id="partner-img" name="partner-image" accept="image/*"  class="form-control mb-2"  multiple hidden/>
                                </label> 
                            @error('image')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </section>
</div>
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }
  $("#partner-img").change(function() {
    readURL(this);
  });
  $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img  class="img-display" style=" width:10%; padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#partner-img').change(function(){
        imagesPreview(this,'div.img-main');
    });
});
</script>
 @endsection