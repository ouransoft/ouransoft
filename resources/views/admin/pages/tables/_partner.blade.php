@extends('admin/index3')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lý đối tác</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lý đối tác</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">
              <form action="{{url('admin/partner/insert')}}" method="POST" enctype="multipart/form-data" runat="server">
                  {{csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="partner-name">Tên đối tác</label>
                    <input type="text" name="name" class="form-control mb-2" id="partner-name1" placeholder="Nhập tên đối tác">
                    @error('name')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="partner-link">Link</label>
                    <input type="text" name="link" class="form-control mb-2" id="partner-link1" placeholder="Nhập link trang web">
                    @error('link')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                      <label for="exampleInputFile">Hình ảnh</label>
                      <br>                 
                      <div class="img-main " style="    border: 2px dashed #0087F7; border-radius: 5px;">
                        <img class="img-main" /> 
                      </div>
                      <label for="partner-img" class="btn btn-info form-control"><i class="fas fa-upload"></i>Choose a file
                        <input type='file' id="partner-img" name="partner-image" accept="image/*" class="form-control mb-2"  multiple hidden/>
                      </label> 
                    @error('image')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
              </form> 
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead style="text-align: center">
                        <tr>
                        <th>Tên đối tác</th>
                        <th>Hình ảnh</th>
                        <th>Link</th>
                        <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($records as $record)
                      <tr id="partner-del{{$record->id}}">
                        <td style="text-align: center; width:25%">{{$record->name }}</td>
                        <td style="text-align: center;  width:25%" ><img src="/upload/partner/{{$record->image}}" alt="1" width="50%"></td>
                        <td style="text-align: center;  width:25%">{{$record->link}}</td>
                        <td style="text-align: center ;  width:25%">
                          <a class="btn btn-info" href="{{url('admin/partner/edit/'.$record->id)}}"><i class="fas fa-pencil-alt"></i></a>
                          <a data-id="{{$record->id}}" class=" Partner-delete btn btn-danger" href=""><i class="fas fa-times-circle"></i></a>
                        </td>
                    </tr>
                      @endforeach
                    </tbody>
                </table>
              </div>
              <!-- Modal -->
      
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
    //delete-partner
    $('.Partner-delete').click(function(){
        const id = $(this).data('id');
        var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
        if(cfrm == true){
        $.ajax({
          url: "/admin/partner/delete/"+id,
          method: "get",
            data: {
            'id' : id,
          },
          succes:function(){
            console.log('#partner-del'+id);
              $('#partner-del'+id).remove();
          }
        });
      }
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
    $("#partner-img").change(function() {
      readURL(this);
    });
    //
    $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img  class="img-display" style=" width:10%; padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#partner-img').change(function(){
        imagesPreview(this,'div.img-main');
    });
});
  </script>
@endsection
