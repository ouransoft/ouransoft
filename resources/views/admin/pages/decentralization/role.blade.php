@extends('admin/index3')
@section('content')
{{--  --}}
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Phân quyền</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Phân quyền</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
{{--  --}}
    <form action="{{url('/admin/permission/insert')}}" method="post" >
    {{csrf_field()}}
        <div class="card-body">
          <div class="form-group">
            <label for="role-name">Vai trò</label>
            <input type="text" name="name" class="form-control mb-2" id="permission-title1" placeholder="Nhập vai trò">
            @error('name')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="role-display-name">Tên hiển thị </label>
            <input type="text" name="display_name" class="form-control mb-2" id="permission-title1" placeholder="Nhập tên">
            @error('display_name')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group ">
            <label for="permission-name">Quyền hạn</label>
              <div class="input-group row ">
                @foreach ($permission as $permission)
                <div class="main navbar col-md-3">
                    <div class="nav-item ">  
                        <input name="permission[]" type="checkbox"  value="{{$permission->id}}">
                        {{trans('translate.'.$permission->name)}}
                    </div>   
                </div> 
                @endforeach  
              </div> 

            @error('permission')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Thêm mới</button>
        </div>
  </form>
  <div class="card-body"> 
    <table id="example1" class="table table-bordered table-striped">
      <thead style="text-align: center">
          <tr>
            <th>Vai trò </th>
            <th>Tên hiển thị</th>
        
            <th>Tác vụ</th>
          </tr>
      </thead>
      <tbody>
        @foreach($role as $role)
          <tr id="role-permission{{$role->id}}" >
              <td style="text-align:center; width:25%;">{{$role->name}}</td>
              <td style="text-align:center; width:25%;">{{$role->display_name}}</td>
           
                @foreach($role->permissions as $permission)
                    <input type="hidden" {{$permission->name}}>
                 @endforeach
          
              <td style="text-align:center; width:25%;">
                <a class="btn btn-info" href="{{url('admin/permission/edit/'.$role->id)}}" ><i class="fas fa-pencil-alt"></i></a>
                <a class="btn btn-danger role-delete" data-id="{{$role->id}}"><i class="fas fa-times-circle"></i></a>
              </td>
          </tr>
          @endforeach
      </tbody>
    </table>
  </div>
  <script>
    $('.role-delete').click(function(){
        const id = $(this).data('id');
        var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
        if(cfrm == true){
        $.ajax({
          url: "/admin/permission/delete/"+id,
          method: "get",
          success: function(){
              console.log('#role-permission'+id);
              $('#role-permission'+id).remove();
          }
        });
      }
    });
  </script>
  </div>
  @endsection
 