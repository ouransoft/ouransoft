@extends('admin/index3')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quản lí người dùng</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quản lí người dùng</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <form action="{{url('admin/user/update')}}" method="post">
                {{csrf_field()}}
                    <div class="card-body">
                        <input type="hidden" name="id" class="form-control mb-2"  value="{{$user->id}}">
                            <div class="form-group">
                                <label for="decentralization-name">Tên thành viên</label>
                                    <input type="text" name="name" class="form-control mb-2" id="" placeholder="Nhập tên" value="{{$user->name}}">
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>        
                            <div class="form-group">
                                <label for="decentralization-email">Email</label>
                                    <input type="text" name="email" class="form-control mb-2" placeholder="Nhập Email" value="{{$user->email}}">
                                    @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-group">
                                <label for="decentralization-passd">Mật khẩu</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="password" name="password" class="form-control mb-2" placeholder="Nhập mật khẩu" value="">
                                        </div>
                                    </div>
                                    @error('password')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-group ">
                                <label for="cars" >Quyền hạn</label>
                                  <div class="input-group">
                                      <div class="custom-file">
                                        <select name="role[]" id="cars"  class="form-control mb-2" >
                                            @foreach ($role as $role)    
                                              <option value="{{$role->id}}" @if(in_array($role->id,$user->roles()->pluck('id')->toArray()))  checked @endif>
                                                  {{$role->name}}
                                              </option>
                                            @endforeach
                                      </select>
                                      </div>
                                    </div>
                                @error('role')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                    </div>
            
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </section>
</div>

  @endsection