@extends('admin/index3')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Quản lý người dùng</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Quản lý người dùng</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- /.card -->
          <div class="card">
            <form action="{{route('admin.user.insert')}}" method="post" >
              {{csrf_field()}}
                  <div class="card-body">
                    <div class="form-group">
                      <label for="decentralization-name">Tên thành viên</label>
                      <input type="text" name="name" class="form-control mb-2"  placeholder="Nhập tên">
                      @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="decentralization-email">Email</label>
                      <input type="text" name="email" class="form-control mb-2" placeholder="Nhập Email">
                      @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="decentralization-passd">Mật khẩu</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="password" name="password" class="form-control mb-2" placeholder="Nhập mật khẩu">
                        </div>
                      </div>
                      @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>

                    <div class="form-group">
                      <label for="decentralization-role">Vai trò</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <select name="role[]" id="cars"  class="form-control mb-2" >
                              @foreach ($role as $role)    
                                <option value="{{$role->id}}" >{{$role->name}}</option>
                              @endforeach
                        </select>
                        </div>
                      </div>
                      @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    
                  </div>
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                  </div>
            </form>
            <div class="card-body"> 
              <table id="example1" class="table table-bordered table-striped">
                <thead style="text-align: center">
                    <tr>
                      <th>Tên </th>
                      <th>Email</th>
                      <th>Vai trò</th>
                      <th>Tác vụ</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($records as $record)
                    <tr id= "user-role-{{$record->id}}">
                        <td style="text-align:center; width:20%;">{{$record->name}}</td>
                        <td style="text-align:center ; width:20%;">{{$record->email}}</td>
                        <td  style="text-align:center;  width:20%;">
                          @foreach($record->roles as $role)
                               <p>  {{$role->name}}</p>
                            @endforeach
                        </td>
                        <td style="text-align:center; width:20%;">
                          <a class="btn btn-info" href="{{url('admin/user/edit/'.$record->id)}}" ><i class="fas fa-pencil-alt"></i></a>
                          <a class="btn btn-danger user-delete" data-id="{{$record->id}}"><i class="fas fa-times-circle"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- Modal -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
 
<script>
  $('.user-delete').click(function(){
        const id = $(this).data('id');
        var cfrm = confirm("Bạn có chắc chắn muốn xóa ?");
        if(cfrm == true){
        $.ajax({
          url: "/admin/user/delete/"+id,
          method: "get",
          success: function(){
              console.log('#user-role-'+id);
              $('#user-role-'+id).remove();
          }
        });
      }
    });
</script>
@endsection

