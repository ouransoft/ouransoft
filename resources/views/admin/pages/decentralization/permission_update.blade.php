@extends('admin/index3')
@section('content')
{{--  --}}
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Phân quyền</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Phân quyền</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
{{--  --}}
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-info">
          <form action="{{url('/admin/permission/update')}}" method="post" >
          {{csrf_field()}}
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" name="id" class="form-control mb-2"  value="{{$role->id}}">
                  <label for="decentralization-name">Vai trò</label>
                  <input type="text" name="name" class="form-control mb-2" id="permission-title1"  value="{{$role->name}}">
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="decentralization-name">Tên hiển thị </label>
                  <input type="text" name="display_name" class="form-control mb-2" id="permission-title1"   value="{{$role->display_name}}">
                  @error('display_name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group ">
                  <label for="decentralization-passd">Quyền hạn</label>
                    <div class="input-group row ">
                      @foreach ($permission as $permission)
                      <div class="main navbar col-md-3">
                          <div class="nav-item  ">  
                              <input name="permission[]" type="checkbox"  value="{{$permission->id}}"  
                              @if (in_array($permission->id, $role->permissions()->pluck('id')->toArray())) checked @endif>
                              {{trans('translate.'.$permission->name)}}
                          </div>   
                      </div> 
                      @endforeach  
                    </div> 

                  @error('permission')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
        </form>
      </div>
    </div>
  </div>
</section>
</div>
  @endsection