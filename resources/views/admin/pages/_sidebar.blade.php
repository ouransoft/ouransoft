<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/admin" class="brand-link">
        <img src="/dist/img/AdminLTELogo.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Ouransoft</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <p style="color: #fff">{{Auth::user()->name}}</p>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{url('/admin')}}" class="nav-link {{ Request::is('admin') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.webconfig')}}" class="nav-link {{ Request::is('admin/webConfig') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Cấu hình web</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.subcriber')}}" class="nav-link {{ Request::is('admin/subcriber') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Thông tin đăng ký</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.news')}}" class="nav-link {{ Request::is('admin/news') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quản lý tin tức</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.document')}}" class="nav-link {{ Request::is('admin/document') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quản lý tài liệu</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.partner')}}" class="nav-link {{ Request::is('admin/partner') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quản lý đối tác</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.product')}}" class="nav-link {{ Request::is('admin/product') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quản lý sản phẩm</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.menu')}}" class="nav-link {{ Request::is('admin/menu') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quản lý menu</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.contact')}}" class="nav-link {{ Request::is('admin/contact') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quản lý liên hệ</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.user')}}" class="nav-link {{ Request::is('admin/user') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quản lý người dùng</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.permission')}}" class="nav-link {{ Request::is('admin/permission') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quản lý phân quyền</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>