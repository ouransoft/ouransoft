-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 05, 2021 lúc 03:32 AM
-- Phiên bản máy phục vụ: 10.4.17-MariaDB
-- Phiên bản PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ouransoft`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Nguyen Tuan anh', 'tuananh31020@gmail.com', 'a', '2021-03-16 21:57:53', '2021-03-16 21:57:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `documents`
--

INSERT INTO `documents` (`id`, `title`, `image`, `file`, `ordering`, `created_at`, `updated_at`) VALUES
(2, 'anh3', '1617433749.png', '1617423327.txt,', 1, '2021-03-16 21:59:33', '2021-04-03 00:09:09'),
(3, 'anh3', '1617435870.png', '1617435870.png,', 2, '2021-04-03 00:44:30', '2021-04-03 00:44:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` smallint(6) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`id`, `title`, `ordering`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Trang chủ', 1, '/', '2021-03-16 20:40:18', '2021-04-04 03:29:08'),
(2, 'Giới thiệu', 2, 'introduce_company', '2021-03-16 20:40:28', '2021-03-17 00:34:06'),
(3, 'Đào tạo', 3, 'training', '2021-03-16 20:40:37', '2021-03-17 00:34:11'),
(4, 'Tin tức', 4, 'news', '2021-03-16 20:40:48', '2021-03-17 00:34:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` smallint(6) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'news-banner1.jpg',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metatitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakeyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_public` timestamp NOT NULL DEFAULT current_timestamp(),
  `timer` date NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `title`, `category`, `description`, `ordering`, `image`, `content`, `metatitle`, `metadescription`, `metakeyword`, `date_public`, `timer`, `created_at`, `updated_at`) VALUES
(7, 'Chuyển đổi số trong doanh nghiệp khoa học công nghệ 1', 'tin vắn', 'mô tả 1', 2, '1617585120.jpg', '<p>jjlk</p>', 'v', 'c', 'c2', '2021-03-17 07:21:28', '2021-03-17', '2021-03-17 00:21:28', '2021-04-04 18:12:00'),
(19, 'a12', 'as', 'a', 1, '1617533933.jpg', '<p>dasdsa</p>', 'a', 'a', 'a', '2021-04-03 04:05:21', '2021-04-20', '2021-04-02 21:05:21', '2021-04-04 03:58:53'),
(21, 'a', 'a', 'a', 1, '1617533942.jpg', '<p>đ&acirc;s</p>', '8', '123', '123', '2021-04-03 04:51:09', '2021-03-31', '2021-04-02 21:51:09', '2021-04-04 03:59:02');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `partners`
--

INSERT INTO `partners` (`id`, `name`, `image`, `link`, `created_at`, `updated_at`) VALUES
(1, 'tuan anh1234', '1615972009.jpg', 'trangchu.php', '2021-03-16 20:26:48', '2021-04-03 19:34:41'),
(2, 'Google', '1615973903.jpg', 'gg.com', '2021-03-17 02:38:23', '2021-03-17 02:38:23'),
(3, 'FPT', '1615973912.png', 'fpt.com', '2021-03-17 02:38:32', '2021-03-17 02:38:32'),
(4, 'DELL', '1615973919.png', 'dell.com', '2021-03-17 02:38:39', '2021-03-17 02:38:39'),
(5, 'VNP', '1615973933.jpg', 'vnp.com', '2021-03-17 02:38:53', '2021-03-17 02:38:53'),
(6, 'HP', '1615973951.jpg', 'hp.com', '2021-03-17 02:39:11', '2021-03-17 02:39:11'),
(7, 'VNG', '1615973963.jpg', 'vng.com', '2021-03-17 02:39:23', '2021-04-03 02:52:36'),
(8, 'SAMSUNG', '1615974001.jpg', 'samsung.com', '2021-03-17 02:40:01', '2021-04-03 08:02:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', '$2y$10$kzP0kyIgZJEfu9RXl8JSSehmX4kf03hO59Ers4AcZSVEiITdAtFye', '2021-03-19 18:16:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `route`, `created_at`, `updated_at`) VALUES
(4, 'subcriber_view', 'admin.subcriber', NULL, NULL),
(5, 'subcriber_delete', 'admin.subcriber.delete', NULL, NULL),
(6, 'partner_view', 'admin.partner', NULL, NULL),
(7, 'partner_insert', 'admin.partner.insert', NULL, NULL),
(8, 'partner_edit', 'admin.partner.edit', NULL, NULL),
(10, 'partner_delete', 'admin.partner.delete', NULL, NULL),
(11, 'webconfig_view', 'admin.webconfig', NULL, NULL),
(12, 'webconfig_edit', 'admin.webconfig.edit', NULL, NULL),
(14, 'document_view', 'admin.document', NULL, NULL),
(15, 'document_edit', 'admin.document.edit', NULL, NULL),
(17, 'document_delete', 'admin.document.delete', NULL, NULL),
(18, 'document_insert', 'admin.document.insert', NULL, NULL),
(19, 'menu_view', 'admin.menu', NULL, NULL),
(20, 'menu_insert', 'admin.menu.insert', NULL, NULL),
(21, 'menu_edit', 'admin.menu.edit', NULL, NULL),
(23, 'menu_delete', 'admin.menu.delete', NULL, NULL),
(24, 'news_view', 'admin.news', NULL, NULL),
(25, 'news_insert', 'admin.news.insert', NULL, NULL),
(28, 'news_edit', 'admin.news.edit', NULL, NULL),
(30, 'news_delete', 'admin.news.delete', NULL, NULL),
(31, 'product_view', 'admin.product', NULL, NULL),
(32, 'product_insert', 'admin.product.insert', NULL, NULL),
(33, 'product_edit', 'admin.product.edit', NULL, NULL),
(35, 'product_delete', 'admin.product.delete', NULL, NULL),
(36, 'contact_view', 'admin.contact', NULL, NULL),
(37, 'contact_delete', 'admin.contact.delete', NULL, NULL),
(38, 'user_view', 'admin.user', NULL, NULL),
(39, 'user_insert', 'admin.user.insert', NULL, NULL),
(41, 'user_edit', 'admin.user.edit', NULL, NULL),
(43, 'user_delete', 'admin.user.delete', NULL, NULL),
(44, 'permission_view', 'admin.permission', NULL, NULL),
(45, 'permission_insert', 'admin.permission.insert', NULL, NULL),
(46, 'permission_edit', 'admin.permission.edit', NULL, NULL),
(48, 'permission_delete', 'admin.permission.delete', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_roles`
--

CREATE TABLE `permission_roles` (
  `permission_id` int(11) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `permission_roles`
--

INSERT INTO `permission_roles` (`permission_id`, `role_id`) VALUES
(1, 52),
(6, 52),
(4, 53),
(5, 53),
(6, 53),
(7, 53),
(8, 53),
(9, 53),
(10, 53),
(11, 53),
(4, 55),
(9, 55),
(12, 53),
(13, 53),
(14, 53),
(15, 53),
(16, 53),
(17, 53),
(18, 53),
(19, 53),
(20, 53),
(21, 53),
(22, 53),
(23, 53),
(24, 53),
(25, 53),
(26, 53),
(27, 53),
(28, 53),
(29, 53),
(30, 53),
(31, 53),
(32, 53),
(33, 53),
(34, 53),
(35, 53),
(36, 53),
(37, 53),
(38, 53),
(39, 53),
(41, 53),
(42, 53),
(43, 53),
(44, 53),
(45, 53),
(46, 53),
(48, 53),
(14, 54),
(15, 54),
(17, 54),
(18, 54),
(19, 54),
(20, 54),
(21, 54),
(23, 54),
(24, 54),
(25, 54),
(28, 54),
(30, 54),
(4, 56),
(5, 56),
(6, 56),
(7, 56),
(8, 56),
(9, 56),
(10, 56),
(11, 56),
(12, 56),
(13, 56),
(31, 56),
(32, 56),
(33, 56),
(34, 56),
(35, 56),
(36, 56),
(37, 56),
(4, 57),
(5, 57),
(4, 58),
(5, 58),
(6, 58),
(7, 58),
(8, 58),
(9, 58),
(10, 58),
(11, 58),
(12, 58),
(13, 58),
(19, 58),
(20, 58),
(21, 58),
(22, 58),
(23, 58),
(31, 58),
(32, 58),
(33, 58),
(34, 58),
(35, 58),
(36, 58),
(37, 58),
(49, 53),
(24, 59),
(25, 59),
(28, 59),
(29, 59),
(30, 59),
(16, 60),
(17, 60),
(14, 58),
(15, 58),
(16, 58),
(17, 58),
(18, 58),
(24, 58),
(28, 58),
(4, 61);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `content`, `description`, `created_at`, `updated_at`) VALUES
(3, 'Mado-car', '1615973832.png', 'Chuyên đáp ứng dịch vụ, nhu cầu  cho chuyên viên cao cấp', 'Mobile Application', '2021-03-17 02:37:12', '2021-03-17 02:37:12'),
(26, 'Kaizen', '1617534014.png', 'Giúp doanh nghiệp huấn luyện chuẩn hóa nhận thức về PDCA', 'Website Application', '2021-04-04 04:00:14', '2021-04-04 04:00:14'),
(27, 'Mado-car', '1617534069.png', 'Chuyên đáp ứng nhu cầu của chuyên viên cao cấp', 'Mobile Application', '2021-04-04 04:01:09', '2021-04-04 04:01:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `updated_at`, `created_at`) VALUES
(53, 'Amin', 'Quản lí', '2021-03-28 19:16:24', '2021-03-28 18:24:30'),
(54, 'Editor', 'Người viết bài', '2021-03-28 19:16:10', '2021-03-28 19:06:56'),
(58, 'Collaborators', 'Cộng tác viên', '2021-03-29 02:17:03', '2021-03-29 02:17:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_user`
--

CREATE TABLE `role_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(38, 2),
(38, 3),
(39, 2),
(39, 3),
(39, 4),
(39, 5),
(39, 6),
(39, 7),
(39, 8),
(39, 9),
(39, 10),
(40, 2),
(40, 3),
(41, 2),
(41, 3),
(42, 2),
(43, 2),
(43, 3),
(43, 4),
(44, 2),
(46, 3),
(47, 2),
(47, 3),
(48, 3),
(49, 3),
(49, 4),
(49, 5),
(49, 6),
(49, 2),
(45, 2),
(50, 11),
(55, 3),
(65, 3),
(67, 53),
(68, 54),
(69, 54),
(70, 54),
(72, 57),
(73, 54),
(74, 56),
(75, 53),
(76, 53),
(77, 53),
(78, 54),
(79, 56),
(80, 56),
(81, 54),
(82, 56),
(83, 53),
(71, 58),
(84, 59),
(85, 60);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `subcribers`
--

CREATE TABLE `subcribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `subcribers`
--

INSERT INTO `subcribers` (`id`, `date`, `email`, `created_at`, `updated_at`) VALUES
(2, '2021-03-29 07:27:16', '123@gmail.com', '2021-03-29 00:27:16', '2021-03-29 00:27:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(67, 'admin', 'admin@gmail.com', NULL, '$2y$10$4J43HfQEz44Wwqcros3AU.KZF5VLRSDqAUScAXNNS4ABA.KTZNWOi', NULL, '2021-03-28 18:25:18', '2021-03-28 18:40:28'),
(68, 'Editor', 'editor@gmail.com', NULL, '$2y$10$wpBftKtlCplHq4feUQxaje.wnjtOVq4yfIbrZI0HZcD/7bjOPICnG', NULL, '2021-03-28 19:07:14', '2021-03-28 19:07:14'),
(71, 'Collaborators', 'ctv@gmail.com', NULL, '$2y$10$dGwNnJRosVM/Ho9ti5bz3exuK27Xyc2EVznDB3xWpPoTSKbSuSHoC', NULL, '2021-03-28 23:54:11', '2021-03-30 20:03:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `web_configs`
--

CREATE TABLE `web_configs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `favicon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `web_configs`
--

INSERT INTO `web_configs` (`id`, `name`, `logo`, `favicon`, `address`, `tel`, `description`, `sub_description`, `facebook`, `twitter`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'trang chủ', '1617553461.png', '1617553450.png', 'Số 3, lô Mở rộng, Trung Hành 5 Đằng Lâm, Hải An, Hải Phòng', '0833.993.966', '323232323', '32323111', '#', '#', '#1', NULL, '2021-04-04 09:24:21');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `subcribers`
--
ALTER TABLE `subcribers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `web_configs`
--
ALTER TABLE `web_configs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT cho bảng `subcribers`
--
ALTER TABLE `subcribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT cho bảng `web_configs`
--
ALTER TABLE `web_configs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
