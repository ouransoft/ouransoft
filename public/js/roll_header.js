// var $animation_elements = $('.hotline-bar');
// var $animation_elements_2 = $('.main-intro');
// var $animation_elements_3 = $('#introduce .solution');
// var $animation_elements_4 = $('.section5 .news-wrap .content');
// var $window = $(window);

// function check_if_in_view(element,classname) {
//     var window_height = $window.height();
//     var window_top_position = $window.scrollTop();
//     var window_bottom_position = (window_top_position + window_height);

//     $.each(element, function() {
        
//         var $element = $(this);
//         var element_height = $element.outerHeight();
//         var element_top_position = $element.offset().top;
//         var element_bottom_position = (element_top_position + element_height);
    
//         //check to see if this current container is within viewport
//         if ((element_bottom_position >= window_top_position) &&
//             (element_top_position <= window_bottom_position)) {
//             $element.addClass(classname);
//         } else {
//             $element.removeClass(classname);
//         }
//     });
// }
// $window.on('scroll resize', function(){
//     check_if_in_view($animation_elements,'fly');
//     check_if_in_view($animation_elements_2,'about-intro-in');
//     check_if_in_view($animation_elements_3,'fly-in');
//     check_if_in_view($animation_elements_4,'fly-in');
// });
// $window.trigger('scroll');

// $('.main .nav-link').click(function(){
//     console.log("danhan");
//     $(this).addClass('present-page');
// });

// $(".nav .nav-item").on("click", function(){
//     $(".nav").find(".active-page").removeClass("active-page");
//     $(".nav-item").addClass("active-page");
//  });

var lastScroll = 0;

$(window).on('load',function(){
    $('header').addClass('header-appear');
});

$(document).on('scroll', function(){
    let currentScroll = document.documentElement.scrollTop || document.body.scrollTop; // Get Current Scroll Value

    if (currentScroll > 0 && lastScroll <= currentScroll){
        lastScroll = currentScroll;
        $('.topbar').addClass('hide');
    }else{
        lastScroll = currentScroll;
        $('.topbar').removeClass('hide');
    }

    if ( $(window).scrollTop() > 50) {
        $('.wrap-navbar').addClass('change-color');
    } else {
        $('.wrap-navbar').removeClass('change-color');
    }
});

$('.navbar-toggler').on('click', function(){
    $('.wrap-navbar').toggleClass('change-color-2');
});
