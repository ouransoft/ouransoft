$('.slider').slick({
	arrows:true,
  infinite:true,
  slidesToShow:3,
  accessibility:true,
  autoplay:true,
  autoplaySpeed:3000,
  responsive: [
    {
      breakpoint: 980,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
			

