<div class="section1 pb-5">
    <div class="about-us pt-5">
        <h2 class="py-3 mx-auto"><span>ABOUT</span> OURANSOFT</h2>
    </div>
    <div class="about-company mt-5 d-flex flex-row mx-auto">
        <div class="left">
            <div id="carouselExampleIndicators" class="company-slide carousel slide" data-ride="carousel">
                <ol class="carousel-indicators mt-3">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="my-auto active"><p>Danh dự</p></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" class="mx-2 my-auto"><p>Tâm huyết</p></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2" class="my-auto"><p>Sáng tạo</p></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="item-content">
                            <div class="item-inner my-auto mx-auto d-flex flex-column justify-content-center">
                                <p><span class="material-icons pr-2">add_task</span>Đáp ứng lòng tin</p>
                                <p><span class="material-icons pr-2">add_task</span>Cam kết bằng danh dự</p>
                            </div>
                        </div>
                        <img src="upload/honour.jpg" class="d-block w-100" alt="honour">
                    </div>
                    <div class="carousel-item">
                        <div class="item-content">
                            <div class="item-inner my-auto mx-auto d-flex flex-column justify-content-center">
                                <p><span class="material-icons pr-2">add_task</span>Luôn luôn nhiệt huyết</p>
                                <p><span class="material-icons pr-2">add_task</span>Quyết tâm thực hiện triệt để</p>
                            </div>
                        </div>  
                        <img src="upload/heart.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <div class="item-content">
                            <div class="item-inner my-auto mx-auto d-flex flex-column justify-content-center">
                                <p><span class="material-icons pr-2">add_task</span>Sáng tạo táo bạo</p>
                                <p><span class="material-icons pr-2">add_task</span>Hướng tới giá trị mới</p>
                            </div>
                        </div>
                        <img src="upload/creatively.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
            </div>
        </div>
        <div class="right d-flex flex-column">
            <div class="about-intro">
                <div class="main-intro ">
                    <h2 class="ml-3 py-2">Ouransoft Technology JSC</h2>
                </div>
            </div>
            <div class="about-content mx-auto mt-5">
                <p class="main-content">
                    <span class="pl-4">Ouransoft Technology Jsc.</span> ra đời với mục đích và tôn chỉ hàng đầu là xây dựng một phong cách phục vụ đẳng cấp, đáp ứng ngày càng nhiều nhu cầu của Quý khách hàng với thời gian nhanh nhất, chất lượng cao nhất.
                </p>
            </div>
            <div class="sub-content mx-auto mt-4">
                <span>
                    “ Niềm tin của khách hàng là giá trị của chúng tôi”
                </span>
            </div>
            <div class="about-more-info mx-auto mt-5">
                <a href="#" class="mx-auto py-1 d-flex justify-content-between align-items-center">
                    <span class="content">Xem thêm</span>
                    <span class="material-icons mr-1">arrow_right_alt</span>
                </a>
            </div>
        </div>
    </div>
    <div class="page-break d-flex flex-row mt-5">
        <div class="rectangle rectangle1"></div>
        <div class="rectangle rectangle2"></div>
        sdsadasdasdsa
    </div>
</div>