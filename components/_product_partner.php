<div class="background-partner section3" > 
	<div class="coating-1" >
		<div class="container product-main-info">
			<div class="control-solution py-3">
				<h2 class="mx-auto py-3">HIGHLIGHTS PRODUCTS</h2>
				<hr>
				<span>
					<p>Sản phẩm nổi bật</p>
				</span>
			</div>
			<div class="grid-x grid-margin-x slider align-items-center ">
				<div class="cell-auto-slides col-md-4">
					<img src="./upload/madocar.jpg" alt="" title="1">
					<div class="infor-products" style="text-align:center;">
						<div class="name-products">
							<h4 >	Mado-car</h4>
						</div>
						<div class="description-products">
							<ul>
								<b>
									<li>Đáp ứng hầu hết nhu cầu của chuyên viên cấp cao</li>
									<br>
									<li>Giúp chuyên viên rút ngắn thời gian chờ đợi</li>
									<br>
									<li> Thông tin của quý khách hàng luôn được bảo mật </li>
									<br>
								</b>	
							</ul>
						</div>
					</div>
				</div>
				<div class="cell-auto-slides col-md-4">
					<img src="./upload/logo_kaizen.jpg" alt="d" title="2" >
					<div class="infor-products" style="text-align:center; margin-top: 5.5%;">
						<div class="name-products">
							<h4 >Kaizen</h4>
						</div>
						<div class="description-products">
							<ul>
								<b>	
									<li>Giúp DN huấn luyện chuẩn hóa nhận thức về PDCA</li>
									<br>
									<li>Giúp DN số hóa quy trình, tạo ngân hàng ý tưởng</li>
									<br>
									<li>Giúp CNV quản lí lịch trình, sắp xếp công việc </li>
									<br>
								</b>	
							</ul>	
						</div>
					</div>
				</div>
				<div class="cell-auto-slides col-md-4">
					<img src="./upload/madocar.jpg" alt="" title="1">
					<div class="infor-products" style="text-align:center">
						<div class="name-products">
							<h4 >Mado-car</h4>
						</div>
						<div class="description-products">
							<ul>
								<b>
									<li>Đáp ứng hầu hết nhu cầu của chuyên viên cấp cao</li>
									<br>
									<li>Giúp chuyên viên rút ngắn thời gian chờ đợi</li>
									<br>
									<li> Thông tin của quý khách hàng luôn được bảo mật </li>
									<br>
								</b>	
							</ul>
						</div>
					</div>
				</div>
				<div class="cell-auto-slides col-md-4">
					<img src="./upload/logo_kaizen.jpg" alt="d" title="2" >
					<div class="infor-products" style="text-align:center ;margin-top: 5.5%;" >
						<div class="name-products">
							<h4 >Kaizen</h4>
						</div>
						<div class="description-products">
							<ul>
								<b>	
									<li>Giúp DN huấn luyện chuẩn hóa nhận thức về PDCA</li>
									<br>									
									<li>Giúp DN số hóa quy trình, tạo ngân hàng ý tưởng</li>
									<br>
									<li>Giúp CNV quản lí lịch trình, sắp xếp công việc </li>
									<br>
								</b>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
    </div>
	<div class="container partner-cont">
		<div class="control-solution py-3">
				<h2 class="mx-auto py-3">PARTNER OF OURANSOFT</h2>
			<hr>
			<span>
				<p>Đối tác của chúng tôi</p>
			</span>
		</div> 
		<div class="row partner-rows">  
			<div class="col-md-3 logo-partners-vwit">  
				<div class="title-partner-icon" style="margin-top:10%;">
					<!-- <a href="#"><img src="./upload/569408959.jpg" style="width: 50%; height: auto;"></a> -->
					<a href="#"><img src="./upload/lg_logo.png" style="width: 50%; height: auto;"></a>
				</div>
			</div>     
			<div class="col-md-3 logo-partners-vwit">    
				<div class="title-partner-icon" style="margin-top:10%;">
				<a href="#" >	<img src="./upload/logo-cpn.png" style="width: 60%; height: auto;"></a>
				</div>
			</div>   
			<div class="col-md-3 logo-partners-vwit">    
				<div class="title-partner-icon">
				<a href="#"><img src="./upload/ssg-logo.jpg" style="width: 50%; height: auto;  "></a>
				</div>
			</div> 
			<div class="col-md-3 logo-partners-vwit">    
				<div class="title-partner-icon">
				<a href="#"><img src="./upload/logo-intel-web.png" style="width: 70%; height: auto;  "></a>
				</div>
			</div>   
			<div class="col-md-3 logo-partners-vwit">    
				<div class="title-partner-icon">
					<a href="#"><img src="./upload/mslogo.jpg" style="width: 80%; height: auto;"></a>
				</div>
			</div>     
			<div class="col-md-3 logo-partners-vwit">    
				<div class="title-partner-icon">
					<a href="#"><img src="./upload/logo-hp.jpg" style="width: 40%; height: auto;"></a>
				</div>
			</div>     
			<div class="col-md-3 logo-partners-vwit">    
				<div class="title-partner-icon">
					<a href="#"><img src="./upload/logo-h2c1.png" style=" width: 35%; height: auto;"></a>
				</div>
			</div>
			<div class="col-md-3 logo-partners-vwit">    
				<div class="title-partner-icon">
					<a href="#"><img src="./upload/logodel.png" style=" width: 35%; height: auto;"></a>
				</div>
			</div>        
		</div>
	</div>
</div>
<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
    </div>