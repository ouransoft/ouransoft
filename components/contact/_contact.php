<div class="container" id="contact-page">
    <div class="row py-5">
        <div class="contact-detail col-sm-6">
            <div class="wrap-contact">
                <div class="contact-box container px-5 py-5">
                    <h6 class="pb-2">
                        <i class="fas fa-circle"></i>
                        <span class="pl-4">CONTACT DETAILS</span>
                    </h6>
                    <h2 class="pb-3">Thông tin liên hệ</h2>
                    <p class="pb-4"><b>OURANSOFT TECHNOLOGY JOINT STOCK COMPANY</b></p>
                    <div class="location d-flex pb-2">
                        <i class="contact-icon fas fa-map-marker-alt"></i>
                        <div class="location-text pl-4">
                            <h6>ĐỊA CHỈ VĂN PHÒNG</h6>
                            <p>Số 3, Lô 16 Mở rộng, Trung Hành 5, Đằng Lâm, Hải An, Hải Phòng</p>
                        </div>
                    </div>
                    <div class="email d-flex pb-2">
                        <i class="contact-icon far fa-envelope"></i>
                        <div class="email-text pl-4">
                            <h6>EMAIL</h6>
                            <P>Ouransoft@gmail.com</P>
                        </div>
                    </div>
                    <div class="hotline d-flex pb-2">
                        <i class="contact-icon fas fa-phone-volume"></i>
                        <div class="hotline-text pl-4">
                            <h6>HOTLINE</h6>
                            <P>0936.36.4899 | 083.3993.966</P>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-form col-sm-6">
            <div class="form-contact-box py-5 container">
                <h6 class="pb-2">
                    <i class="fas fa-circle"></i>
                    <span class="pl-4">GET IN TOUCH</span>
                </h6>
                <h2 class="pb-3">Liên hệ với OURANSOFT</h2>
                <form action="#" class="d-flex flex-column">
                    <input class="py-2 pl-4 mb-4" type="text" name="name" placeholder="Họ và tên*">
                    <input class="py-2 pl-4 mb-4" type="text" name="email" placeholder="Email*">
                    <textarea class="py-2 mb-4 pl-4" name="content" cols="30" rows="5" placeholder="Nội dung..."></textarea>
                    <div class="col-lg-4 submit-btn">
                        <a href="#" class="btn btn-info">Đăng ký</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
</div>