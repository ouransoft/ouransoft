<div class="news">
    <div class="news-wrap row my-5 mx-auto d-flex flex-row justify-content-center">
        <div class="news-box mx-4 my-4">
            <div class="inner-box mx-auto d-flex flex-column justify-content-between">
                <div class="time mt-4">
                    <p>
                        <i class="far fa-clock"></i>
                        <span>03/03/2021</span>
                    </p>
                </div>
                <div class="title pb-3 mt-2">
                    <a href="news_detail.php"><h4>Research Paper Writing – Writing Tips</h4></a>
                </div>
                <div class="content">
                    <p>If you are attempting to compose a good research paper, and then...</p>
                </div>
                <div class="read pb-4">
                    <a href="news_detail.php">ĐỌC BÀI<i class="pl-2 fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
        <div class="news-box mx-4 my-4">
            <div class="inner-box mx-auto d-flex flex-column justify-content-between">
                <div class="time mt-4">
                    <p>
                        <i class="far fa-clock"></i>
                        <span>03/03/2021</span>
                    </p>
                </div>
                <div class="title pb-3 mt-2">
                    <a href="news_detail.php"><h4>Research Paper Writing – Writing Tips</h4></a>
                </div>
                <div class="content">
                    <p>If you are attempting to compose a good research paper, and then...</p>
                </div>
                <div class="read pb-4">
                    <a href="news_detail.php">ĐỌC BÀI<i class="pl-2 fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
        <div class="news-box mx-4 my-4">
            <div class="inner-box mx-auto d-flex flex-column justify-content-between">
                <div class="time mt-4">
                    <p>
                        <i class="far fa-clock"></i>
                        <span>03/03/2021</span>
                    </p>
                </div>
                <div class="title pb-3 mt-2">
                    <a href="news_detail.php"><h4>Research Paper Writing</h4></a>
                </div>
                <div class="content">
                    <p>If you are attempting to compose a good research paper, and then...</p>
                </div>
                <div class="read pb-4">
                    <a href="news_detail.php">ĐỌC BÀI<i class="pl-2 fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
        <div class="news-box mx-4 my-4">
            <div class="inner-box mx-auto d-flex flex-column justify-content-between">
                <div class="time mt-4">
                    <p>
                        <i class="far fa-clock"></i>
                        <span>03/03/2021</span>
                    </p>
                </div>
                <div class="title pb-3 mt-2">
                    <a href="news_detail.php"><h4>Research Paper Writing – Writing Tips</h4></a>
                </div>
                <div class="content">
                    <p>If you are attempting to compose a good research paper, and then...</p>
                </div>
                <div class="read pb-4">
                    <a href="news_detail.php">ĐỌC BÀI<i class="pl-2 fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
        <div class="news-box mx-4 my-4">
            <div class="inner-box mx-auto d-flex flex-column justify-content-between">
                <div class="time mt-4">
                    <p>
                        <i class="far fa-clock"></i>
                        <span>03/03/2021</span>
                    </p>
                </div>
                <div class="title pb-3 mt-2">
                    <a href="news_detail.php"><h4>Research Paper Writing – Writing Tips</h4></a>
                </div>
                <div class="content">
                    <p>If you are attempting to compose a good research paper, and then...</p>
                </div>
                <div class="read pb-4">
                    <a href="news_detail.php">ĐỌC BÀI<i class="pl-2 fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
    </div>