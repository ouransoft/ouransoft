<div class="introduce-quality" style="background-color: #f2f6fd; padding-top:5%; ">
         <div class= container>
            <div class="row">
                <div class="col-md-6 intro-quali-detail" style="display: block;">
                <h4 style="color: #f17521; text-align: center; "  ><b>VISION & MISSION</b></h4>
                <h4 style="color: #f17521;  text-align: center; "><b>Tầm nhìn & sứ mệnh</b></h4>
                <hr>
                    <ul >
                        <div class="intro-qual-NDT" style="height: 100%; padding: 3%;">
                        <b style=" color: #f17521;">  <i class="fas fa-record-vinyl" id="NDT" ></i>      Nhà đầu tư</b>
                        <li id="NDTt" style="padding: 1%;"><b>Bằng sự trân trọng và biết ơn, chúng tôi sẽ đem lại những sự phát triển bền vững và những giá trị gia tăng mới cho các nhà đầu tư và các đối tác</b></li>
                        </div>
                        <br>
                        <div class="intro-qual-KH" style="height: 100%; padding: 3%;">
                        <b style="padding: 1%; color: #f17521;"> <i class="fas fa-record-vinyl"id="KH" ></i> Khách hàng</b>
                        <li id="KHt" style="margin-left:5%;" ><b>Bằng sự thấu hiểu, đồng cảm chúng tôi sẽ cung cấp cho khách hàng những giải pháp, dịch vụ chất lượng, đúng thời điểm, an toàn và thân thiện.</b></li>
                        </div>
                        <br>
                        <div class="intro-qual-CNV" style="height: 100%; padding: 3%;">
                        <b style="padding: 1%; color: #f17521;"><i class="fas fa-record-vinyl"id="CNV"></i> Công nhân viên</b>
                        <li id="CNVt" style="margin-left:5%;"> <b>Bằng sự tôn trọng các cá nhân, chúng tôi đang từng ngày cố gắng xây dựng công ty thành nơi công nhân viên xây dựng ước mở của chính mình.</b></li>
                        </div>
                        <br>
                        <div class="intro-qual-XH" style="height: 100% ;padding: 3%;">
                        <b style="padding: 1%; color: #f17521;"><i class="fas fa-record-vinyl"id="XH" ></i> Xã hội</b>
                        <li id="XHt"style="margin-left:5%;"><b>Bằng danh dự, nhiệt huyết và sự sáng tạo, chúng tôi hiểu rằng mình phải có trách nhiệm và nghĩa vụ đem lại sự phát triển và hạnh phúc cho xã hội.</b></li>
                        </div>
                    </ul>
                </div>
                <div class="col-md-6 intro-quali-img" style="margin-top:15%; display:block;">
                         <img  class=" surface" src="./upload/about-image2-1.png" style="opacity: 1;transform: matrix(1, 0, 0, 1, 0, 0);">
                </div>
        </div>
    </div>
    <script>
 $("#NDT").click(function(){
    $("#NDTt").toggle("slow");
});
$("#KH").click(function(){
    $("#KHt").toggle("slow");
});
$("#CNV").click(function(){
    $("#CNVt").toggle("slow");
});
$("#XH").click(function(){
    $("#XHt").toggle("slow");
});
</script>   

</div>   
<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
    </div>
 