
<div class="introduce-start" style="background-color:#f2f6fd;" >
    <div class="container">
		    <div class="row"  >
			      <div class="col-md-6 intro-start-img" style="margin-top: 15%;">
				        <img  class=" surface" src="./upload/what-we-offer-background.png" style="opacity: 1;transform: matrix(1, 0, 0, 1, 0, 0);">
			      </div>
			      <div class="col-md-6 introduce-start-detail">
                <h5 style="color: #f17521; text-align:center; "><i class="fas fa-question-circle"></i><b> WHO WE ARE</b></h5>
                <h5 style="color: #f17521; text-align:center; "><b>OURANSOFT TECHNOLOGY JSC</b></h5>
              <hr>
				        <div class="content-introduce-head">
                  <ul>
                    <div class=intro-head-first > <b>  <li>OURANSOFT TECHNOLOGY JSC là công ty trong lĩnh vực cung cấp dịch vụ tư vấn, thiết kế các giải pháp tổng thể và lắp đặt các hệ thống máy tính, mạng không dây, nhà thông minh, bảo vệ giám sát các công trình dân dụng, công nghiệp bằng công nghệ thông minh.</li></b></div>
                    <br>
                    <div class=intro-head-second> <b>  <li> Trong xu thế toàn cầu hoá, chúng tôi luôn đảm bảo gia tăng giá trị và giúp cho Quý khách hàng tránh khỏi những rủi ro không mong muốn và dù ở trong hay ngoài  nước Quý khách hàng vẫn có thể theo sát và quản lý công việc một cách hiệu quả nhất.</li></b></div>
                    <br>
                    <div class="intro-head-third"> <b><li>Với phương châm “ Niềm tin của khách hàng là giá trị của chúng tôi”, chúng tôi đã và đang không ngừng nỗ lực hoàn thiện mình để đem lại sự hài lòng của Quý khách hàng, và sự nỗ lực đổi mới với mong muốn sẽ được góp phần vào sự phát triển trong tương lai của Quý khách hàng.</li></b></div>
                  </ul>
                </div>
			      </div>	
		    </div>
	  </div>
</div>
<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
    </div>