<header>
    <div class="content">
        <div class="sub-content">
            <div class="slogan px-4">
                <h1>INTRODUCE </h1>
            </div>
            <div class="sub-slogan ">
               <ul style="display: flex; list-style: none; ">
                    <li>
                        <a href="index.php" style="color:#a5b7d2;">Trang chủ </a>
                     <i class="fas fa-angle-right" style="color:#a5b7d2;"></i>
                        Giới thiệu
                    </li>    
                </ul>
            </div>
        </div>
    </div>
    <div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1"></div>
        <div class="rectangle rectangle2"></div>
    </div>
</header>