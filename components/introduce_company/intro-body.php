
<div class="introduce-character"  >
        <div class="container heading-value-title"    >
            <div class="title-value-head">                 
                    <h4 style="color: #f17521;  text-align:center; padding-top:3%"  ><b> CORE VALUES</b></h4>
                    <h4  style="color: #f17521;  text-align:center"><b>Giá trị cốt lõi</b></h4>                     
                </div>
                <hr> 
            </div>          
         <div class= "container core-value-detail">  
        
                <div class="row main-value-detail" style="text-align:center; padding-top:3%">
                        <div class="col-md-4 hexa-right-main" >
                           <div class="logo-honor-value"> <div  id="hexagon-right"><i class="fas fa-award" style="font-size:60px; color: white; "></i> </div>  </div>
                            <br>
                                <h4 style=" color: #f17521; padding-top: 2%;"> <b>Danh dự - HONOUR</b></h4>
                               <h5 style="padding-top: 2%; ; "><b>Luôn không ngừng hoàn thiện bản thân và đáp ứng lòng tin</b></h5>
                            </div>
                            <br>
                        <div class="col-md-4 hexa-left-main">
                           <div class="logo-honor-value" > <div id="hexagon" ><i class="fas fa-hand-holding-heart" style="font-size:60px; color: white; "></i></div></div>
                            <br>
                                <h4 style=" color: #f17521; padding-top: 2%;"><b>Nhiệt huyết - HEART</b></h4>
                               <h5 style="padding-top: 2% ;"><b>Luôn nhiệt huyết, hết mình, quyết tâm thực hiện triệt để</b></h5>  
                            </div>
                        <div class="col-md-4 hexa-center-main">
                          <div class="logo-honor-value " >  <div id="hexagon-center" ><i class="far fa-lightbulb" style="font-size:60px; color:white;"></i></div></div>
                            <br>
                       <h4 style=" color: #f17521;padding-top: 2%;"><b>Sáng tạo - CREATIVITY</b></h4>
                            <h5 style="padding-top: 2% ; "><b>Sáng tạo, táo bạo, khác biệt, hướng tới những giá trị mới</b>   </h5>  
                        </div>     
                </div>
                <!-- <hr>        -->
        </div>

</div>
<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
    </div>
