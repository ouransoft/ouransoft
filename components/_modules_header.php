<header>
    <div class="content">
        <div class="sub-content">
            <div class="slogan px-4">
                <h1>INTRODUCE </h1>
            </div>
            <div class="sub-slogan ">
                <h6>
                    <a href="index.php" style="color:#a5b7d2;">Trang chủ </a>
                    <i class="fas fa-angle-right" style="color:#a5b7d2;"></i>
                    Name of page
                </h6>
            </div>
        </div>
    </div>
    <div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1"></div>
        <div class="rectangle rectangle2"></div>
    </div>
</header>