<div class="section2">
    <div id="introduce" class="site-main">
		<div class="container">
			<div class="control-solution py-5">
				<h2 class="mx-auto py-3">CONTROL<span class="first"> - </span>CONNECTIVITY SOLUTION</h2>
				<hr>
				<span>
					<p>Tư vấn giải pháp</p> 
					<p>& Thi công hệ thống</p>
				</span>
			</div>
			<div class="tab-pane fade show active" id="tab-3">
				<div class="row">
					<div class="col">
						<div class="solution show-on_scroll">
							<div class="title-head"> 
								<h5>AUTOMATION PROCESS IN MANUFATORING</h5>
							</div>
							<hr>
							<div class="content-firt-solution" >
								<p>Cung cấp các giải pháp và thiết bị cho hệ thống tự động hóa dây chuyền trong các nhà máy sản xuất</p>
									<b> 
										- PLCs, SCADA, MES, ERP
										<br>
											- Smart Factory
										<br>
										- etc
									</b>
								<p>Giúp giảm lãng phí, nâng cao giá trị gia tăng, từ đó gia tăng lợi nhuận của doanh nghiệp.</p>
							</div>
							<a href="#" class="more-info btn mb-3" style="color: white;">Chi tiết <i class="fas fa-arrow-circle-right"></i></a>
						</div>   
					</div>   
					<div class="col">
						<div class="solution show-on_scroll" >    
							<div class="title-head">
								<h5>CONNECTIVITY & CONTROL IN BUILDING</h5>
							</div>
							<hr>
							<div class="content-firt-solution" >
								<p>Cung cấp các giải pháp và thiết bị giúp kết nối và điều khiển tòa nhà như:</p>
									<b>                  
										- Hệ thống nhà thông minh 
										<br>
											- Hệ thống an ninh thông minh (Smart Security)
										<br>
										- etc
										<br>
									</b>
								<p>Giúp tăng trải nghiệm đẳng cấp và tiện nghi tại các tòa nhà, căn hộ.</p>
							</div>
							<a href="#" class="more-info btn mb-3" style="color: white;">Chi tiết <i class="fas fa-arrow-circle-right"></i></a>
						</div>   
					</div>   
					<div class="col">
						<div class="solution show-on_scroll" > 
							<div class="title-head">
								<h5>MANAGEMENT SYSTEMS TRAINING</h5>
							</div>
							<hr>
							<div class="content-firt-solution" >
								<p>Cung cấp các khóa đào tạo cho hệ thống nhân sự điều hành tại doanh nghiệp, nhà máy như:</p>
									<b>
										- PDCA, Kaizen, 5S, 4M, MBO, KPI
										<br>
										- 5P, PEST, 3C, BCM, 7SM
										<br>
										- etc
										<br>
									</b>
								<p>Giúp nâng cao kỹ năng tinh giảm bộ máy quản lý, đem lại hiệu quả và sự phát triển bền vững của doanh nghiệp</p>
							</div>
							<a href="#" class="more-info btn mb-3" style="color: white;">Chi tiết <i class="fas fa-arrow-circle-right"></i></a>
						</div>   
					</div>   
				</div>   
			</div>
		</div>
    </div>
	<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1"></div>
        <div class="rectangle rectangle2"></div>
    </div>
</div>