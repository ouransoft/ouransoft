<header>
    <div class="content">
        <div class="sub-content">
            <div class="slogan px-4">
                <h1>SLOGANSLO</h1>
            </div>
            <div class="sub-slogan px-4">
                <h4>Sub slogan</h4>
            </div>
        </div>
    </div>
    <div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1"></div>
        <div class="rectangle rectangle2"></div>
    </div>
</header>