<footer id="footer" >	
	<div class="container">
		<div class="infomation">	
			<div class="row main">
				<div class="hotline-bar" >
					<div class="container ">					
						<div class="row item">
							<div class="col-lg-4" style="color:white; margin-top:2.3%; left:3% ; opacity:1;">
								<h4> Subscribe for Newsletter</h4>
								<p> Đăng ký nhận tin tức và được tư vấn</p>
							</div>
							<div class="col-lg-6 " style="margin-top:3%; left:1%;">
								<span class="wpcf7-form-control-wrap your-tel">
								<input type="tel" name="your-tel" value="" size="80" class="form-control"  placeholder="Nhập Email hoặc số điện thoại của bạn..." >
							</div>
							<div class="col-lg-2" style="margin-top:3%; left:2%; ">
								<a href="#" class="btn btn-info">Đăng ký</a>
							</div>						
						</div>		
					</div>		
				</div>
				<div class="col-md-3 footer-ani">
					<div class="image-footer">
                        <img src="upload/logo-ouransoft.png" style=" width:170px;  color:white !important;">
                    </div>		
                    <br>
                    <h4 style="color:#ff6e00e6; margin-left:3%; text-align: center; font-size: 15px;"><strong> OURANSOFT TECHNOLOGY JSC</strong></h4>
                </div>	
                <div class="col-md-3 footer-ani">
                    <div class="infomain">
                        <h4>Thông tin liên hệ</h4>
                        <ul>
                            <li><a href="#" style="color:white;"><i class="fas fa-map-marker-alt "style=" color:white;"></i> Số 3, lô Mở rộng,Trung Hành 5 Đằng Lâm, Hải An, Hải Phòng</a></li>
                            <li><a href="#" style="color:white;"><i class="fas fa-phone" ></i> 0225.8838.122</a></li>
                            <li><a href="#" style="color:white;"><i class="fas fa-mobile-alt"></i> 083.3993.966</a></li>
                            <li><a href="#" style="color:white;"><i class="fab fa-internet-explorer" ></i> www.ouransoft.vn </a></li>	
                            <li><a href="#" style="color:white;"><i class="far fa-envelope" ></i> Ouransoft@gmail.com</a></li>         
                            <div class="Social-network">
                                <ul>
                                    <li><a href="#" style="color:white;"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#" style="color:white;"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#" style="color:white;"><i class="fab fa-instagram"></i></a></li>
                                </ul>	
                            </div>	
                        </ul>
                    </div>	
                </div>
                <div class="col-md-3 footer-ani">
                    <div class="service">
                        <h4>Dịch vụ</h4>
                        <ul>
                            <li><a href="#"class=" elementor-icon-list-text " style="   color:white;"><i class="fas fa-concierge-bell" style="color: white;"></i> SCADA - Smart Factory</a></li>
                            <li><a href="#"style="   color:white;" ><i class="fas fa-concierge-bell" style="color: white;" ></i> HR &amp; Management Training</a></li>
                            <li ><a href="#" style="   color:white;" ><i class="fas fa-concierge-bell" style="color: white;"></i> Kaizen Website</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 footer-ani">
                    <div class="other-info">
                        <h4>Khác</h4>	
                        <ul>
                            <li><a href="#" style="color:white;"><i class="fas fa-user-friends"></i> Thông tin đối tác </a></li>
                            <li><a href="#" style="color:white;"><i class="fab fa-product-hunt"></i> Thông tin sản phẩm  </a></li>
                            <li><a href="#" style="color:white;"><i class="fas fa-list"></i> Chính sách và quy định chung</a></li>
                            <li><a href="#" style="color:white;"> <i class="fas fa-shield-alt"></i> Chính sách bảo mật</a></li>
                        </ul>	
                    </div>
                </div>
            </div>
        </div>
        <div class="wrap-headline">
            <hr>
                <ul class="text-center">
                <li class="list-inline-item" >Copyright © 2020 OURANSOFT. All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</footer>


