<div class="training-page py-5">
    <div class="row py-5">
        <div class="col-lg-4 left d-flex flex-column justify-content-center">
            <div class="box1 mb-5">
                <div class="service-box mx-5">
                    <div class="inner-box container pb-4 px-4">
                        <i class="fas fa-stream my-4"></i>
                        <h5 class="pb-2">Hệ thống 5S</h5>
                        <p>Sàng lọc - Sắp xếp - Sạch sẽ - Săn sóc - Sẵn sàng</p>
                        <span class="big-number">01</span>
                    </div>
                </div>
            </div>
            <div class="box2 mb-5">
                <div class="service-box mx-5">
                    <div class="inner-box container pb-4 px-4">
                        <i class="fas fa-file-signature my-4"></i>
                        <h5 class="pb-2">Hệ thống Kaizen</h5>
                        <p>Với hệ thống Kaizen, mỗi công nhân trong nhà máy luôn thực hiện công việc một cách dễ dàng, đơn giản.</p>
                        <span class="big-number">02</span>
                    </div>
                </div>
            </div>
            <div class="box3 mb-5">
                <div class="service-box mx-5">
                    <div class="inner-box container pb-4 px-4">
                        <i class="fas fa-chart-pie my-4"></i>
                        <h5 class="pb-2">PDCA</h5>
                        <p>PDCA giúp cải thiện hiệu suất quá trình một cách ổn định và có tổ chức trong qua các giai đoạn Lập kế hoạch – Thực hiện – Kiểm tra và Hành động.</p>
                        <span class="big-number">03</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 center">
            <div class="d-flex flex-column justify-content-center h-100">
                <div class="title pb-5">
                    <h6 class="pb-2 d-flex align-items-center justify-content-center">
                        <i class="fas fa-circle"></i>
                        <span class="px-4">HR & MANAGEMENT TRAINING</span>
                        <i class="fas fa-circle"></i>
                    </h6>
                    <h2 class="pb-3">Đào tạo nguồn nhân lực</h2>
                </div>
                <div class="training-img">
                    <img src="upload/training-center.png" alt="training">
                </div>
            </div>
        </div>
        <div class="col-lg-4 right d-flex flex-column justify-content-center">
            <div class="box1 mb-5">
                <div class="service-box mx-5">
                    <div class="inner-box container pb-4 px-4">
                        <i class="fas fa-angle-double-down my-4"></i>
                        <h5 class="pb-2">Teamwork</h5>
                        <p>Làm việc theo nhóm giúp chúng ta hoàn thành công việc hiệu quả hơn.</p>
                        <span class="big-number">04</span>
                    </div>
                </div>
            </div>
            <div class="box2 mb-5">
                <div class="service-box mx-5">
                    <div class="inner-box container pb-4 px-4">
                        <i class="far fa-star my-4"></i>
                        <h5 class="pb-2">Cap-do</h5>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <span class="big-number">05</span>
                    </div>
                </div>
            </div>
            <div class="box3 mb-5">
                <div class="service-box mx-5">
                    <div class="inner-box container pb-4 px-4">
                        <i class="fas fa-cloud-upload-alt my-4"></i>
                        <h5 class="pb-2">???</h5>
                        <p>Xây dựng hệ thống quản lý máy móc thiết bị.</p>
                        <span class="big-number">06</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
    </div>