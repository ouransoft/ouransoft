<div class="news-detail-wrap my-5 mx-auto row container">
    <div class="col-sm-8 search-result">
        <div class="result-box mx-auto mb-5 px-4 py-4">
            <div class="time">
                <p>
                    <i class="far fa-clock"></i>    
                    <span>03/03/2021</span>
                </p>  
            </div>
            <div class="title mb-3">
                <a href="#"><h3>ssay Helper Versus Essay Writer</h3></a>
            </div>
            <div class="content">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto, assumenda enim nesciunt accusantium reiciendis deserunt iste dolores, excepturi consequuntur, vel repudiandae nostrum quos ut facere facilis? Tempora aliquid officiis obcaecati?</p>
                <p>Modi laboriosam nihil tenetur autem tempora accusamus deserunt provident obcaecati quia tempore, laborum numquam error delectus minus aperiam consequuntur sunt voluptatum velit, facere vitae vero. Magnam nobis eveniet impedit! Rem[...]</p>
            </div>
        </div>
        <div class="result-box mx-auto mb-5 px-4 py-4">
            <div class="time">
                <p>
                    <i class="far fa-clock"></i>    
                    <span>03/03/2021</span>
                </p>  
            </div>
            <div class="title mb-3">
                <a href="#"><h3>ssay Helper Versus Essay Writer</h3></a>
            </div>
            <div class="content">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto, assumenda enim nesciunt accusantium reiciendis deserunt iste dolores, excepturi consequuntur, vel repudiandae nostrum quos ut facere facilis? Tempora aliquid officiis obcaecati?</p>
                <p>Modi laboriosam nihil tenetur autem tempora accusamus deserunt provident obcaecati quia tempore, laborum numquam error delectus minus aperiam consequuntur sunt voluptatum velit, facere vitae vero. Magnam nobis eveniet impedit! Rem[...]</p>
            </div>
        </div>
        <div class="result-box mx-auto mb-5 px-4 py-4">
            <div class="time">
                <p>
                    <i class="far fa-clock"></i>    
                    <span>03/03/2021</span>
                </p>  
            </div>
            <div class="title mb-3">
                <a href="#"><h3>ssay Helper Versus Essay Writer</h3></a>
            </div>
            <div class="content">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto, assumenda enim nesciunt accusantium reiciendis deserunt iste dolores, excepturi consequuntur, vel repudiandae nostrum quos ut facere facilis? Tempora aliquid officiis obcaecati?</p>
                <p>Modi laboriosam nihil tenetur autem tempora accusamus deserunt provident obcaecati quia tempore, laborum numquam error delectus minus aperiam consequuntur sunt voluptatum velit, facere vitae vero. Magnam nobis eveniet impedit! Rem[...]</p>
            </div>
        </div>
    </div>
    <div class="col-sm-4 news-menu">
        <div class="menu-box d-flex flex-column">
            <div class="search mb-4">
                <h4 class="mb-3">Tìm kiếm</h4>
                <div class="search-box pl-3">
                    <input class="pl-2 pr-5" type="text" placeholder="Tìm kiếm..."/>
                    <button><i class="fas fa-search pr-3"></i></button>
                </div>
            </div>
            <div class="selection">
                <h4 class="mb-3">Bài viết mới nhất</h4>
                <div class="selection-box pl-3">
                    <ul>
                        <li><a href="#">Spend loans are high-risk choices for borrowers day.</a></li>
                    </ul>
                    <ul>
                        <li><a href="#">Most useful Wi-Fi speakers of 2020: Apple, Sonos, Polk and Ikea. Cordless sound for the home has existed for the time that is long</a></li>
                    </ul>
                    <ul>
                        <li><a href="#">5 Positions To Simply Help You Join The Mile-High Club</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-break d-flex flex-row">
        <div class="rectangle rectangle1" style=" background-color: #f17521;
    width: 50%; height:3px;"></div>
        <div class="rectangle rectangle2" style=" background-color: black;
    width: 50%;"></div>
</div>