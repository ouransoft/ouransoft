<div class="wrap-navbar navbar-expand-lg">
    <div class="topbar">
        <div class="topbar-wrap mx-auto row py-3">
            <div class="social-media col row">
                <a href="#"><i class="fab fa-twitter-square mx-3"></i></a>
                <a href="#"><i class="fab fa-facebook-f mx-3"></i></a>
                <a href="#"><i class="fab fa-instagram mx-3"></i></a>
            </div>
            <div class="address col-md-auto d-flex flex-row">
                <i class="fas fa-map-marker-alt"></i>
                <p class="pl-2">Số 3, lô Mở rộng, Trung Hành 5 Đằng Lâm, Hải An, Hải Phòng</p>
            </div>
            <div class="contact col-lg-2 d-flex flex-row justify-content-center">
                <i class="fas fa-phone"></i>
                <p class="pl-2">0833.993.966</p>
            </div>
        </div>
    </div>
    <nav class="main navbar mx-auto">
        <div class="logo">
            <a class="navbar-brand" href="index.php">
                <img src="upload/logo.png" alt="logo">
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="material-icons">
                menu
            </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item py-3">
                    <a class="nav-link" href="index.php">Trang chủ</a>
                </li>
                <li class="nav-item py-3">
                    <a class="nav-link" href="introduce_company.php">Giới thiệu</a>
                </li>
                <li class="nav-item py-3">
                    <a class="nav-link" href="training.php">Đào tạo</a>
                </li>
                <li class="nav-item py-3">
                    <a class="nav-link" href="news.php">Tin tức</a>
                </li>
            </ul>
            <div class="search-box pr-4">
                <form action="#" method="post" class="form-inline my-2 my-lg-0">
                    <div class="d-flex flex-row frame">
                        <input class="search-area" type="search" placeholder="Tìm kiếm..." aria-label="Search">
                        <button class="search-icon">
                            <span class="material-icons pt-1">
                                search
                            </span>
                        </button>
                    </div>
                </form>
            </div>
            <div class="contact pl-4">
                <span class="nav-item">
                    <a href="contact.php">
                        Liên hệ
                    </a>
                </span>
            </div>
        </div>
    </nav>
</div>

