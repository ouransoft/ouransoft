<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::middleware(['lang'])->group(function(){
    Route::get('/lang/{lang}', 'LocalizationController@index')->name('trans.english');
});


/* Route index */
Route::get('/introduce_company','IntroduceCompanyController@index')->name('introduce');

Route::get('/contact','ContactController@index')->name('contact');
Route::post('/contact/insert','ContactController@insert')->name('contact.insert');

Route::get('/news','NewsController@index')->name('news');
Route::get('/news_detail/{id}','NewsDetailController@index');
Route::get('/faqs','HelpController@faqs')->name('faqs');
Route::post('/search','SearchController@index');

// Route::get('/admin', function() {
//     return view('admin/pages/tables/_dashboard');
// });


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->group(function(){
    /* Route admin */
    Route::get('/admin/logout','Auth\AuthenticatedSessionController@destroy')->name('admin.logout');

    Route::get('/admin','Admin\DashboardController@index');

    Route::middleware(['check.permission'])->group(function(){

        //webConfig
            Route::get('/admin/webConfig','Admin\WebConfigController@index')->name('admin.webconfig');
            Route::get('/admin/webconfig/edit','Admin\WebConfigController@edit')->name('admin.webconfig.edit');
            Route::post('/admin/webconfig/update','Admin\WebConfigController@update')->name('admin.webconfig.edit');
            Route::get('admin/webconfig/updateConfigSuccess','Admin\WebConfigController@index')->name('admin.webconfig.edit');
        //subcriber
            Route::get('/admin/subcriber','Admin\SubcribeController@index')->name('admin.subcriber');
            Route::post('/admin/subcriber/insert','Admin\SubcribeController@insert')->name('admin.subcriber.insert');
            Route::get('/admin/subcriber/delete/{id}','Admin\SubcribeController@delete')->name('admin.subcriber.delete');
        //partner
            Route::get('/admin/partner','Admin\PartnerController@index')->name('admin.partner');
            Route::post('admin/partner/insert','Admin\PartnerController@insert')->name('admin.partner.insert');
            Route::get('/admin/partner/edit/{id}','Admin\PartnerController@edit')->name('admin.partner.edit');
            Route::post('/admin/partner/update','Admin\PartnerController@update')->name('admin.partner.edit');
            Route::get('/admin/partner/delete/{id}','Admin\PartnerController@delete')->name('admin.partner.delete');
        //document
            Route::get('/admin/document','Admin\DocumentController@index')->name('admin.document');
            Route::post('/admin/document/insert','Admin\DocumentController@insert')->name('admin.document.insert');
            Route::get('/admin/document/edit/{id}','Admin\DocumentController@edit')->name('admin.document.edit');
            Route::post('/admin/document/update','Admin\DocumentController@update')->name('admin.document.edit');
            Route::get('/admin/document/delete/{id}','Admin\DocumentController@delete')->name('admin.document.delete');
        //menu
            Route::get('/admin/menu','Admin\MenuController@index')->name('admin.menu');
            Route::post('/admin/menu/insert','Admin\MenuController@insert')->name('admin.menu.insert');
            Route::get('/admin/menu/edit/{id}','Admin\MenuController@edit')->name('admin.menu.edit');
            Route::post('/admin/menu/update','Admin\MenuController@update')->name('admin.menu.edit');
            Route::get('/admin/menu/delete/{id}','Admin\MenuController@delete')->name('admin.menu.delete');
        //news
            Route::get('/admin/news','Admin\NewsController@index')->name('admin.news');
            Route::get('/admin/news/insert','Admin\NewsController@insert')->name('admin.news.insert');
            Route::post('/admin/news/insert', 'CKEditorController@upload')->name('admin.ckeditor.upload');
            Route::post('/admin/news/store','Admin\NewsController@store')->name('admin.news.insert');
            Route::get('/admin/news/edit/{id}','Admin\NewsController@edit')->name('admin.news.edit');
            Route::post('/admin/news/update','Admin\NewsController@update')->name('admin.news.edit');
            Route::get('/admin/news/delete/{id}','Admin\NewsController@delete')->name('admin.news.delete');
        //product
            Route::get('/admin/product','Admin\ProductController@index')->name('admin.product');
            Route::post('/admin/product/insert','Admin\ProductController@insert')->name('admin.product.insert');
            // Route::post('/admin/product/insert', 'CKEditorController@uploadProduct')->name('admin.ckeditor.upload');
            Route::get('/admin/product/edit/{id}','Admin\ProductController@edit')->name('admin.product.edit');
            Route::post('/admin/product/update','Admin\ProductController@update')->name('admin.product.edit');
            Route::get('/admin/product/delete/{id}','Admin\ProductController@delete')->name('admin.product.delete');
        //Contact
            Route::get('/admin/contact','Admin\ContactController@index')->name('admin.contact');
            Route::get('/admin/contact/delete/{id}','Admin\ContactController@delete')->name('admin.contact.delete');
        // //user

            Route::get('admin/user','Admin\UserController@index')->name('admin.user');
            Route::post('/admin/user/insert','Admin\UserController@insert')->name('admin.user.insert');
            Route::get('admin/user/edit/{id}','Admin\UserController@edit')->name('admin.user.edit');
            Route::post('/admin/user/update','Admin\UserController@update')->name('admin.user.edit');
            Route::get('/admin/user/delete/{id}','Admin\UserController@delete')->name('admin.user.delete');
        //permission
            Route::get('/admin/permission','Admin\PermissionController@index')->name('admin.permission');
            Route::post('/admin/permission/insert','Admin\PermissionController@insert')->name('admin.permission.insert');
            Route::get('admin/permission/edit/{id}','Admin\PermissionController@edit')->name('admin.permission.edit');
            Route::post('/admin/permission/update','Admin\PermissionController@update')->name('admin.permission.edit');
            Route::get('/admin/permission/delete/{id}','Admin\PermissionController@delete')->name('admin.permission.delete');
    });


});
require __DIR__.'/auth.php';
