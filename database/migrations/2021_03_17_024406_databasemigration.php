<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Databasemigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_configs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('logo');
            $table->string('favicon');
            $table->string('address');
            $table->string('tel');
            $table->string('description');
            $table->string('sub_description');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('instagram');
        });

        Schema::create('subcribers', function (Blueprint $table) {
            $table->id();
            $table->timestamp('date');
            $table->string('email');
        });

        Schema::create('partners', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image');
            $table->string('link');
        }); 

        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('image');
            $table->string('file');
            $table->smallInteger('ordering');
        });

        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image');
            $table->string('content');
            $table->string('description');
        });

        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->smallInteger('ordering');
            $table->string('link');
        });

        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('category');
            $table->string('description');
            $table->smallInteger('ordering');
            $table->string('image')->default("news-banner1.jpg");
            $table->text('content');
            $table->string('metatitle');
            $table->string('metadescription');
            $table->string('metakeyword');
            $table->timestamp('date_public')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->date('timer')->default(NULL);
        });

        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->text('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('web_configs', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('subcribers', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('partners', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('documents', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('menus', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('news', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('news', function (Blueprint $table) {
            $table->date('timer')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
}
