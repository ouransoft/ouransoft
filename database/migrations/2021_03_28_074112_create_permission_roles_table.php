<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionRolesTable extends Migration
{
 
    public function up()
    {
        Schema::create('permission__roles', function (Blueprint $table) {
            $table->id('permission_id');
            $table->id('role_id');
      
        });
    }


    public function down()
    {
        Schema::dropIfExists('permission_roles');
    }
}
