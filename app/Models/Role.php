<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;
use Illuminate\Foundation\Auth\Role as Authenticatable;
use Illuminate\Notifications\Notifiable;
class Role extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'roles';
    protected $fillable = ['id','name','route'];

    public function permissions(){
        return $this->belongsToMany(Permission::class,'permission_roles');
    }
}