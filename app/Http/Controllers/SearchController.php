<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class SearchController extends Controller
{
    function index(Request $request){
        $result = $request->input('search-value');
        $records = News::where('title','like','%'.$result.'%')
                        ->orWhere('content','like','%'.$result.'%')
                        ->orderByDesc('date_public')
                        ->get();
        $news = News::all()->sortBy('ordering')->take(5);
        return view('component/search/search_result', compact('records','news','result'));
    }
}
