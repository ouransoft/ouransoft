<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class NewsDetailController extends Controller
{
    function index($id){
        $records = News::find($id);
        $news = News::all()->sortBy('ordering')->take(5);
        return view('component/news/news_detail', compact('records','news'));
    }
}
