<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Partner;
use App\Models\News;

class HomeController extends Controller
{
    function index(){
        $records_product = Product::all();
        $records_partner = Partner::all();
        $records_news = News::all()->sortBy('ordering')->take(3);

        return view('index')
        ->with(compact('records_product'))
        ->with(compact('records_partner'))
        ->with(compact('records_news'));

    }
}
