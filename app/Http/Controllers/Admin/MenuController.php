<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{
    function index(){
        $records = Menu::all();
        return view('admin/pages/tables/_menu', compact('records'));
    }

    function insert(Request $request){
        $request->validate([
            'title'=>'required',
            'ordering'=>'required',
            'link'=>'required'
        ],
        [
            'title.required'=>'*Chưa nhập tên',
            'link.required'=>'*Chưa nhập đường dẫn',
            'ordering.required'=>'*Chưa chọn ảnh'
        ]);

        $title = $request->input('title');
        $link = $request->input('link');
        $ordering = $request->input('ordering');

        $data = array(
            'title' => $title,
            'link' => $link,
            'ordering' => $ordering
        );

        Menu::create($data);

        return redirect()->route('admin.menu');
    }

    function edit($id){
        $menu = Menu::find($id);
        $records =  Menu::all();  
        return view('admin.pages.tables._menu_update',compact('menu','records'));
   
    }

    function update(Request $request, Menu $menu){
        $id = $request->input('menu-id');
        $title = $request->input('menu-title');
        $link = $request->input('menu-link');
        $ordering = $request->input('menu-ordering');

        $data = array(
            'title' => $title,
            'link' => $link,
            'ordering' => $ordering
        );
        $menu->where('id',$id)->update($data);

        return redirect()->route('admin.menu');
    }

    function delete($id, Menu $menu){
        $menu->where('id',$id)->delete();

        return response()->json([
            'success' => 'Menu has been deleted'
        ]);
    }
}

