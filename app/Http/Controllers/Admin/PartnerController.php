<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Partner;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $records= Partner::all();  
        return view('admin/pages/tables/_partner',compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin\pages\tables\_partner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return 
     */
    public function insert(Request $request)
    {
        $partner = new Partner;
        $this->validate($request,[
            'name'=>'required',
            'partner-image'=>'required',
            'link'=>'required'
        ],
        [
            'name.required'=>'*Chưa nhập tên',
            'link.required'=>'*Chưa nhập đường dẫn',
            'partner-image.required'=>'*Chưa chọn ảnh'
        ]);
        
        if($request->hasfile('partner-image')){
            $filename= $request->file('partner-image');
            $partner->image = time().'.'.$filename->getClientOriginalExtension();
            $path = $filename->move(public_path('upload/partner'), $partner->image);
        }
        else{
            $partner->image ='noimage.jpg';
        }

        $partner->name= $request->input('name');
        $partner->link= $request->input('link');
        $partner->save();
        return redirect()->route('admin.partner');
    }
    
    public function edit($id)
    {
        $partner = Partner::find($id);  
        $records =  Partner::all();  
        return view('admin.pages.tables._partner_update',compact('partner','records'));
    }   

    public function update(Request $request, Partner $partner)
    {
        $id = $request->input('partner-id');
        $name = $request->input('name');
        $link = $request->input('link');

        $data = array(
            'name' => $name,
            'link' => $link
        );
        
        if($request->hasFile('partner-image')){
            $image = $request->file('partner-image');
            $destinationPath = public_path('upload/partner');
            $image_url = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_url);
            $data['image'] = $image_url;
        }
        $partner->where('id',$id)->update($data);
        return redirect()->route('admin.partner');
    }

    public function delete($id, Partner $partner){
        $partner->where('id',$id)->delete();

        return response()->json([
            'success' => 'Menu has been deleted'
        ]);
      
    }
}
