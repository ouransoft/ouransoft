<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Rule;
use App\Models\Role;
use DB;
class UserController extends Controller
{
    public function index(){
        $records = User::all();
        $role= Role::all();

        return view('admin/pages/decentralization/users', compact('records','role'));
    }
    public function insert(Request $request){
        $insert = new User;
        $role= new Role;
        $this->validate($request,[
            'name'=>'required|unique:users',
            'email'=>'required|unique:users',
            'password'=>'required',
            'role'=>'required'
        ],
            [
                'name.required'=>'*Chưa nhập tên',
                'email.required'=>'*Chưa nhập email',
                'passsword.required'=>'*Chưa nhập mật khẩu',
                'role.required'=>'*Chưa chọn quyền',
            ]);
        $insert->name= $request->input('name');
        $insert->email= $request->input('email');
        $insert->password= $request->input('password');
        $insert->password= bcrypt($request->input('password'));
        $role->id=$request->input('role');
        $insert->save();
        $insert->roles()->attach($request->input('role'));
        return redirect()->route('admin.user');
    }
    public function edit($id, Role $role ){
            $user = User::find($id);
            $records = User::all();
             $role =$user->roles()->get();
             $role= Role::all();                                    
            return view('admin.pages.decentralization.user_update',compact('user','role','records'));
    }
    public function update(Request $request, User $user, Role $role){
        
        $id= $request->input('id');
        $name= $request->input('name');
        $email= $request->input('email');
        $password= $request->input('password');
        $password= bcrypt($request->input('password'));     
        $role=$request->input('role'); 
        $data= array(
            'name'=>$name,
            'email'=>$email,
            'password'=>$password,
            'role'=>$role
        ); 
        $this->validate($request,[
            'password'=>'required',
            'role'=>'required'
        ],[
            'password.required'=>'Chưa nhập mật khẩu',
            'role.required'=>'Chưa chọn quyền hạn'
        ]);
        $users = $user->find($id);
        $users->update($data);
        $users->roles()->sync($role);
        return redirect()->route('admin.user');
    }
   public function delete($id, User $user){
            $user->where('id',$id)->delete();
            return response()->json([
                'success'=>'Acccount has been delete'
            ]); 
    }   
}
