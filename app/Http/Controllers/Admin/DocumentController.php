<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Document;

class DocumentController extends Controller
{
    function index(){
        $records = Document::all();
        return view('admin/pages/tables/_document', compact('records'));
    }

    function insert(Request $request){
        
        $request->validate([
            'document-title' => 'required',
            'document-ordering' => 'required|numeric',
            'document-image' => 'required',
            'files' => 'required'
        ],
        [
            'document-title.required'=>'*Chưa nhập tiêu đề',
            'document-ordering.required'=>'*Chưa nhập thứ tự sắp xếp',
            'document-image.required'=>'*Chưa chọn ảnh',
            'files.required'=>'*Chưa chọn file',
        ]);

        $title = $request->input('document-title');
        $ordering = $request->input('document-ordering');

        if($request->hasFile('document-image')){
            $image = $request->file('document-image');
            $destinationPath = public_path('upload/document/image');
            $image_url = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_url);
        }

        if($request->hasFile('files')){
            $files = $request->file('files');
            $file_value ="";
            foreach($files as $file){
                $destinationPath2 = public_path('upload/document/file');
                $file_url = time().'.'.$file->getClientOriginalExtension();
                $file->move($destinationPath2, $file_url);
                $file_value .= $file_url.",";
            }
        }

        $data = array(
            'title' => $title,
            'ordering' => $ordering,
            'image' => $image_url,
            'file' => $file_value
        );

        Document::create($data);
        return redirect()->route('admin.document');
    }

    function edit($id){
       
        $records = Document::all();     
        $document= Document::find($id);                        
        return view('admin.pages.tables._document_update',compact('records','document'));
    }

    function update(Request $request){
        $id = $request->input('document-id');
        $title = $request->input('document-title');
        $ordering = $request->input('document-ordering'); 
        $data = array(
            'title' => $title,
            'ordering' => $ordering
        );
        if($request->hasFile('document-image')){
            $image = $request->file('document-image');
            $destinationPath = public_path('upload/document/image');
            $image_url = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_url);
            $data['image'] = $image_url;
        }
        if($request->hasFile('files')){
            $files = $request->file('files');
            $file_value ="";
            foreach($files as $file){
                $destinationPath2 = public_path('upload/document/file');
                $file_url = time().'.'.$file->getClientOriginalExtension();
                $file->move($destinationPath2, $file_url);
                $file_value .= $file_url.",";
            }
            $data['file'] = $file_value;
        }
        Document::where('id',$id)->update($data);
        return redirect()->route('admin.document');
    }

    function delete($id, Document $document){
        $document->where('id',$id)->delete();

        return response()->json([
            'success' => 'Document has been deleted'
        ]);
    }
}
