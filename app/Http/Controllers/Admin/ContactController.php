<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    function index(){
        $contact = Contact::orderBy('id','DESC')->get();
        return view('admin/pages/tables/_contact',compact('contact'));
    }

    function delete($id){
        Contact::find($id)->delete($id);
        // $contact->where('id',$id)->delete();
        return response()->json([
            'success' => 'Contact has been deleted'
        ]);
    }
}
