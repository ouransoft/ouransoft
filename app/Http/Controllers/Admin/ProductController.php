<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    function index(){
        $records= Product::all();
        return view('admin/pages/tables/_product',compact('records'));
    }

    function insert(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'image'=>'required',
            'content'=>'required',
            'description'=>'required'
        ],
        [
            'name.required'=>'*Chưa nhập tên',
            'content.required'=>'*Chưa nhập nội dung',
            'image.required'=>'*Chưa chọn ảnh',
            'description.required'=>'*Chưa nhập mô tả'
        ]);

        if($request->hasfile('image')){
            $filename= $request->file('image');
            $image = time().'.'.$filename->getClientOriginalExtension();
            $path = $filename->move(public_path('upload/product'), $image);
        }


        $name= $request->input('name');
        $content= $request->input('content');
        $link= $request->input('link');
        $description= $request->input('description');
        $data = array(
            'name' => $name,
            'content' => $content,
            'description' => $description,
            'image' => $image,
            'link' => $link
        );
        Product::insert($data);
        return redirect()->route('admin.product');
    }
    function edit($id){
        $product= Product::find($id);
        $records = Product::all();
        return view('admin.pages.tables._product_update',compact('records','product'));
    }
    function update(Request $request, Product $product){
        $id = $request->input('product-id');
        $name = $request->input('name');
        $content=  $request->input('content');
        $link = $request->input('link');
        $description = $request->input('description');
        $content_detail = $request->input('content-detail');

        $data = array(
            'name'=> $name,
            'content' =>$content,
            'description'=>$description,
            'link'=>$link,
            'content_detail'=>$content_detail
        );

        if($request->hasFile('image')){
            $image = $request->file('image');
            $destinationPath = public_path('upload/product');
            $image_url = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_url);
            $data['image'] = $image_url;

        }

        $product->where('id',$id)->update($data);
        return redirect()->route('admin.product');
    }

    public function delete($id, Product $product){
        $product->where('id',$id)->delete();

    return response()->json([
        'success' => 'Menu has been deleted'
    ]);
    }
}
