<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\News;
use App\Models\Subcriber;
use App\Models\Contact;

class DashboardController extends Controller
{
    function index(){
        $product = Product::all()->count();
        $news = News::all()->count();
        $subcriber = Subcriber::all()->count();
        $contact = Contact::all()->count();
        return view('admin/pages/tables/_dashboard', compact('product','news','subcriber','contact'));
    }
}
