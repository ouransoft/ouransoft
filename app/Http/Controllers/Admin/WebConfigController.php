<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WebConfig;

class WebConfigController extends Controller
{
    function index() {
        $records = WebConfig::first();
        return view('admin/pages/tables/_webConfig',compact('records'));
    }
    function edit(){
        $records=  WebConfig::first();
        return view('admin/pages/tables/_webConfig_update',compact('records'));
    }
    function update(Request $request, WebConfig $webconfig){
        $description = $request->input('description');
        $sub_description = $request->input('sub_description');
        $address = $request->input('address');
        $tel = $request->input('tel');
        $facebook = $request->input('facebook');
        $twitter = $request->input('twitter');
        $instagram = $request->input('instagram');

        $data = array(
            'description' => $description,
            'sub_description' => $sub_description,
            'address' => $address,
            'tel' => $tel,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram
        );

        if($request->hasFile('logo')){
            $logo = $request->file('logo');
            $destinationPath = public_path('upload/webconfig');
            $logo_url = time().'.'.$logo->getClientOriginalExtension();
            $logo->move($destinationPath, $logo_url);
            $data['logo'] = $logo_url;
        }
        if($request->hasFile('favicon')){
            $favicon = $request->file('favicon');
            $destinationPath = public_path('upload/webconfig');
            $favicon_url = time().'.'.$favicon->getClientOriginalExtension();
            $favicon->move($destinationPath, $favicon_url);
            $data['favicon'] = $favicon_url;
        }

        $webconfig->first()->update($data);  

        return redirect()->route('admin.webconfig');
    }
}
