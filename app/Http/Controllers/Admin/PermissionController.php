<?php

namespace App\Http\Controllers\Admin;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Validation\Rule;
use DB;


class PermissionController extends Controller
{
    
    /////////////////
    public function index(){
        $role = Role::all();
        $permission= Permission::all();
        return view('admin/pages/decentralization/role', compact('role','permission'));
    }
    public function insert(Request $request){
        $role = new Role;
        $permission = new Permission;
        $this->validate($request,[
            'name'=>'required|unique:roles',
            'display_name'=>'required',
            'permission'=>'required'
        ],
            [
               'name.required'=>'*Chưa nhập vai trò',
               'display_name.required'=>'*Chưa nhập tên hiển thị',
                'permission.required'=>'*Chưa chọn quyền',
            ]);
        $role->name= $request->input('name');
        $role->display_name= $request->input('display_name');
        $permission->id=$request->input('permission');
        $role->save();
        $role->permissions()->attach($request->input('permission'));
        return redirect()->route('admin.permission');
    }
    public function edit($id)
    {
        $role = Role::find($id);
        $permission =$role->permissions()->get();
        $permission= Permission::all();                                    
       return view('admin.pages.decentralization.permission_update',compact('permission','role'));
    }
    public function update(Request $request, Role $role, Permission $permission)
    {
        $id= $request->input('id');
        $name= $request->input('name');
        $display_name= $request->input('display_name');    
        $permission=$request->input('permission'); 
        $data= array(
            'name'=>$name,
            'display_name'=>$display_name,
            'permission'=>$permission
        ); 
        $this->validate($request,[
            'name'=>'required',
            'display_name'=>'required',
            'permission'=>'required'
        ],[
            'name.required'=>'Chưa nhập vai trò',
            'display_name.required'=>'Chưa nhập tên hiển thị',
            'permission.required'=>'Chưa chọn quyền hạn'
          
        ]);
        $roles = $role->find($id);
        $roles->update($data);
        $roles->permissions()->sync($permission);
        return redirect()->route('admin.permission');
    }
    public function delete($id, Role $role){
        $role->where('id',$id)->delete();
        return response()->json([
            'success'=>'Acccount has been delete'
        ]); 
    }
}