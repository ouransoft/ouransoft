<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subcriber;
use DB;

class SubcribeController extends Controller
{
    function index() {
        $records = Subcriber::all();
        return view('admin/pages/tables/_subcriber', compact('records'));
    }
    function insert(Request $request)
    {
        $data=array();
        $data['email'] = $request->email;
        if($data['email'] == ""){
            return response()->json([
                'success' => 'False'
            ]);
        }
        Subcriber::create($data);
        return response()->json([
            'success' => 'True'
        ]);
    }
    function delete($id, Subcriber $subcribe){
        $subcribe->where('id',$id)->delete();

        return response()->json([
            'success' => 'Subcribe has been deleted'
        ]);
    }
}
