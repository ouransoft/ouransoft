<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    function index(){
        $records = News::all();
        return view('admin/pages/tables/_news', compact('records'));
    }

    function insert(){
        return view('admin/pages/forms/_editor');
    }

    function store(Request $request){
        
        $validator = $request->validate([
            'news-title'=>'required',
            'news-category'=>'required',
            'news-description'=>'required',
            'news-ordering'=>'required|numeric',
            'news-meta-title'=>'required',
            'news-meta-description'=>'required',
            'news-meta-keyword'=>'required',
            'news-image'=>'required'
        ],
        [
            'news-title.required'=>'*Chưa nhập tiêu đề',
            'news-category.required'=>'*Chưa nhập danh mục',
            'news-description.required'=>'*Chưa nhập mô tả',
            'news-ordering.required'=>'*Chưa nhập thứ tự hiển thị',
            'news-meta-title.required'=>'*Chưa nhập meta title',
            'news-meta-description.required'=>'*Chưa nhập meta description',
            'news-meta-keyword.required'=>'*Chưa nhập meta keyword',
            'news-image.required'=>'*Chưa chọn ảnh'
        ]);
        
        $timer = $request->input('timer');
        $title = $request->input('news-title');
        $category = $request->input('news-category');
        $description = $request->input('news-description');
        $ordering = $request->input('news-ordering');
        $metatitle = $request->input('news-meta-title');
        $metadescription = $request->input('news-meta-description');
        $metakeyword = $request->input('news-meta-keyword');
        $content = $request->input('news-content');

        $data = array(
            'title' => $title,
            'category' => $category,
            'description' => $description,
            'ordering' => $ordering,
            'metatitle' => $metatitle,
            'metadescription' => $metadescription,
            'metakeyword' => $metakeyword,
            'content' => $content
        );
        if($timer != ""){
            $data['timer'] = $timer;
        }

        if($request->hasFile('news-image')){
            $image = $request->file('news-image');
            $destinationPath = public_path('upload/news/image');
            $image_url = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_url);
            $data['image'] = $image_url;
        }
        News::create($data);

        return redirect()->route('admin.news');
    }

    function edit($id){
        $records = News::find($id);
        return view('admin/pages/forms/_editor_update', compact('records'));
    }

    function update(Request $request){
        $id = $request->input('news-id');
        $title = $request->input('news-title');
        $category = $request->input('news-category');
        $description = $request->input('news-description');
        $ordering = $request->input('news-ordering');
        $metatitle = $request->input('news-meta-title');
        $metadescription = $request->input('news-meta-description');
        $metakeyword = $request->input('news-meta-keyword');
        $content = $request->input('news-content');
        $timer = $request->input('timer');

        $data = array(
            'title' => $title,
            'category' => $category,
            'description' => $description,
            'ordering' => $ordering,
            'metatitle' => $metatitle,
            'metadescription' => $metadescription,
            'metakeyword' => $metakeyword,
            'content' => $content
        );
        if($timer != ""){
            $data['timer'] = $timer;
        }

        if($request->hasFile('news-image')){
            $image = $request->file('news-image');
            $destinationPath = public_path('upload/news/image');
            $image_url = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_url);
            $data['image'] = $image_url;
        }
        News::where('id',$id)->update($data);
        // dd($data);
        return redirect()->route('admin.news');
    }

    function delete($id, News $news){
        $news->where('id',$id)->delete();

        return response()->json([
            'success' => 'News has been deleted'
        ]);
    }
}
