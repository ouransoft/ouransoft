<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use DB;
use Redirect;

class ContactController extends Controller
{
    function index(){
        return view('component/contact/contact');
    }
    function insert(Request $request){
        
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->content = $request->content;
        
        if($contact->name == "" || $contact->email == "" || $contact->content == ""){
            return response()->json([
                "success" => 'false'
            ]);
        }

        $contact->save();
        return response()->json($contact);
    }
    
}
