<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    function index(){
        $records = News::where('timer','<',now())->paginate(9);
        return view('component/news/news', compact('records'));
    }
}
